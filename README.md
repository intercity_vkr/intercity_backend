# IntercityBackend
**Intercity - агрегатор междугородних пассажирских перевозок**
***
## Установка зависимостей
### [Go-migrate](https://github.com/golang-migrate/migrate)
```console
   brew install golang-migrate
```
### [EntGo](https://github.com/ent/ent)
```console
    go install entgo.io/ent/cmd/ent@latest
```
***
## Конфигигурация
### Main config
Расположенение - ```intercity_backend/config/config.yml```
```yaml
http_server:
  port: порт HTTP-сервера
db:
  username: Database username
  password: Database password
  host: Database host
  port: Database port
  db_name: Database name
http_authorization:
  secret_key: Secret key for tokens
  access_token_tl: Access token time life
  refresh_token_tl: Refresh token time life
  auth_header: Header's name for auth
  auth_header_prefix: Header's prefix for auth
```
### Docker-compose file
Расположенение - ```intercity_backend/deploy/docker-compose.yml```
## Запуск
1. Поднять докер-контейнеры
```console
    docker-compose up -d
```
2. Применить миграции(доступно через Makefile)
```console
migrate -database pgx5://intercity_user:intercity_password@localhost:5420/intercity?sslmode=disable -path db/migrations up
```
3. Запустить сервер
```console
    go run ./cmd
```
4. При необходимости можно создать компанию и менеджера
```sql
insert into companies (name, address) values ('ООО "Рога и копыта', 'где у быка рога');

insert into managers (phone, first_name, last_name, company_id)
values ('75555555555', 'Егор', 'Шабалин', (SELECT id FROM companies WHERE name LIKE 'ООО "Рога и копыта'));

```
***
## Команды
### Создать миграцию
```console
migrate create -ext sql -dir db/migrations -seq {имя_миграции}
```
### Добавть Ent-схему
```console
ent new --target internal/dao/schema User
```
### Сгенерировать HTTP-DTO из OpenAPI спецификации
Доступно через Makefile.

**необходимо установить [oapi-codegen](https://github.com/deepmap/oapi-codegen)**
```console
oapi-codegen -generate types openapi/openapi.yml > gen/openapi/intercity.gen.go
```
### Сгенерировать Ent-файлы
Доступно через Makefile
```console
go run -mod=mod entgo.io/ent/cmd/ent generate --target gen/ent ./internal/dao/schema
```
***
## Mocks
### Авторизация менеджера
СМС-код - 5555
### Авторизация пассажира
СМС-код - 5555
***
## USEFUL
### Ent-schema
Расположение - ```intercity_backend/internal/dao/schema```
### Migrations
Расположение - ```intercity_backend/db/migrations```


