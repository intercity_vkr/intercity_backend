// Package openapi provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen/v2 version v2.0.0 DO NOT EDIT.
package openapi

import (
	"time"
)

const (
	ManagerApiKeyScopes   = "ManagerApiKey.Scopes"
	PassengerApiKeyScopes = "PassengerApiKey.Scopes"
)

// Car defines model for Car.
type Car struct {
	// CarId uuid
	CarId        string  `json:"car_id"`
	Color        string  `json:"color"`
	Company      Company `json:"company"`
	Gosnomer     string  `json:"gosnomer"`
	Mark         string  `json:"mark"`
	Model        string  `json:"model"`
	SeatingChart string  `json:"seating_chart"`
}

// CarDeleteRequest defines model for CarDeleteRequest.
type CarDeleteRequest struct {
	// CarId uuid
	CarId string `json:"car_id"`
}

// CarUpdateRequest defines model for CarUpdateRequest.
type CarUpdateRequest struct {
	// CarId uuid
	CarId        string `json:"car_id"`
	Color        string `json:"color"`
	Gosnomer     string `json:"gosnomer"`
	Mark         string `json:"mark"`
	Model        string `json:"model"`
	SeatingChart string `json:"seating_chart"`
}

// Company defines model for Company.
type Company struct {
	Address string `json:"address"`

	// CompanyId uuid
	CompanyId string `json:"company_id"`
	Name      string `json:"name"`
}

// CreateCarRequest defines model for CreateCarRequest.
type CreateCarRequest struct {
	Color        string `json:"color"`
	Gosnomer     string `json:"gosnomer"`
	Mark         string `json:"mark"`
	Model        string `json:"model"`
	SeatingChart string `json:"seating_chart"`
}

// Manager defines model for Manager.
type Manager struct {
	Company   *Company `json:"company,omitempty"`
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`

	// ManagerId uuid
	ManagerId string `json:"manager_id"`
	Phone     string `json:"phone"`
}

// ManagerAuthCodeRequest defines model for ManagerAuthCodeRequest.
type ManagerAuthCodeRequest struct {
	Code string `json:"code"`

	// ManagerId uuid
	ManagerId string `json:"manager_id"`
}

// ManagerAuthInitRequest defines model for ManagerAuthInitRequest.
type ManagerAuthInitRequest struct {
	Phone string `json:"phone"`
}

// ManagerAuthInitResponse defines model for ManagerAuthInitResponse.
type ManagerAuthInitResponse struct {
	// ManagerId uuid
	ManagerId string `json:"manager_id"`
}

// Passenger defines model for Passenger.
type Passenger struct {
	FirstName string  `json:"first_name"`
	LastName  *string `json:"last_name,omitempty"`

	// PassengerId uuid
	PassengerId string `json:"passenger_id"`
	Phone       string `json:"phone"`
}

// PassengerAuthCodeRequest defines model for PassengerAuthCodeRequest.
type PassengerAuthCodeRequest struct {
	Code string `json:"code"`

	// PassengerId uuid
	PassengerId string `json:"passenger_id"`
}

// PassengerAuthInitRequest defines model for PassengerAuthInitRequest.
type PassengerAuthInitRequest struct {
	// FirstName если поле передано - регистрация. Если нет - авторизация
	FirstName *string `json:"first_name,omitempty"`
	LastName  *string `json:"last_name,omitempty"`
	Phone     string  `json:"phone"`
}

// PassengerAuthInitResponse defines model for PassengerAuthInitResponse.
type PassengerAuthInitResponse struct {
	// PassengerId uuid
	PassengerId string `json:"passenger_id"`
}

// PassengerSelfResponse defines model for PassengerSelfResponse.
type PassengerSelfResponse struct {
	FirstName string  `json:"first_name"`
	LastName  *string `json:"last_name,omitempty"`

	// PassengerId uuid
	PassengerId string `json:"passenger_id"`
	Phone       string `json:"phone"`
}

// PassengerTripBookRequest defines model for PassengerTripBookRequest.
type PassengerTripBookRequest struct {
	SeatNumber int `json:"seat_number"`

	// TripId uuid
	TripId string `json:"trip_id"`
}

// PassengerTripFilterRequest defines model for PassengerTripFilterRequest.
type PassengerTripFilterRequest struct {
	// EndPlaceId uuid
	EndPlaceId             string    `json:"end_place_id"`
	EndTime                time.Time `json:"end_time"`
	QuantityAvailableSeats int       `json:"quantity_available_seats"`

	// StartPlaceId uuid
	StartPlaceId string    `json:"start_place_id"`
	StartTime    time.Time `json:"start_time"`
}

// Place defines model for Place.
type Place struct {
	Name string `json:"name"`

	// PlaceId uuid
	PlaceId string `json:"place_id"`
	Point   Point  `json:"point"`
}

// PlaceCreateRequest defines model for PlaceCreateRequest.
type PlaceCreateRequest struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
	Name      string  `json:"name"`
}

// Point defines model for Point.
type Point struct {
	Latitude  float32 `json:"latitude"`
	Longitude float32 `json:"longitude"`
}

// RefreshToken defines model for RefreshToken.
type RefreshToken struct {
	RefreshToken string `json:"refresh_token"`
}

// Route defines model for Route.
type Route struct {
	Description *string `json:"description,omitempty"`
	EndPlace    Place   `json:"end_place"`
	Name        string  `json:"name"`

	// RouteId uuid
	RouteId    string  `json:"route_id"`
	StartPlace Place   `json:"start_place"`
	Stops      []Place `json:"stops"`
}

// RouteCreateRequest defines model for RouteCreateRequest.
type RouteCreateRequest struct {
	Description *string `json:"description,omitempty"`

	// EndPlaceId uuid
	EndPlaceId string `json:"end_place_id"`
	Name       string `json:"name"`

	// StartPlaceId uuid
	StartPlaceId string   `json:"start_place_id"`
	StopsIds     []string `json:"stops_ids"`
}

// RouteDeleteRequest defines model for RouteDeleteRequest.
type RouteDeleteRequest struct {
	// RouteId uuid
	RouteId string `json:"route_id"`
}

// RouteFilterRequest defines model for RouteFilterRequest.
type RouteFilterRequest struct {
	// EndPlaceId uuid
	EndPlaceId string `json:"end_place_id"`

	// StartPlaceId uuid
	StartPlaceId string   `json:"start_place_id"`
	StopsIds     []string `json:"stops_ids"`
}

// RouteUpdateRequest defines model for RouteUpdateRequest.
type RouteUpdateRequest struct {
	Description *string `json:"description,omitempty"`
	Name        string  `json:"name"`

	// RouteId uuid
	RouteId  string   `json:"route_id"`
	StopsIds []string `json:"stops_ids"`
}

// TokenPair defines model for TokenPair.
type TokenPair struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

// Trip defines model for Trip.
type Trip struct {
	Car     Car       `json:"car"`
	Company Company   `json:"company"`
	EndTime time.Time `json:"end_time"`

	// FullPrice в копейках
	FullPrice int `json:"full_price"`

	// Reversed true - в обратном направлении по маршруту
	Reversed     bool      `json:"reversed"`
	Route        Route     `json:"route"`
	SeatingChart string    `json:"seating_chart"`
	StartTime    time.Time `json:"start_time"`

	// TripId uuid
	TripId        string          `json:"trip_id"`
	TripPassenger []TripPassenger `json:"trip_passenger"`
}

// TripCreateRequest defines model for TripCreateRequest.
type TripCreateRequest struct {
	// CarId uuid
	CarId string `json:"car_id"`

	// CustomSeatingChart кастомная рассадка для этого trip
	CustomSeatingChart *string   `json:"custom_seating_chart,omitempty"`
	EndTime            time.Time `json:"end_time"`

	// FullPrice в копейках
	FullPrice int `json:"full_price"`

	// Reversed true - в обратном направлении по маршруту
	Reversed bool `json:"reversed"`

	// RouteId uuid
	RouteId   string    `json:"route_id"`
	StartTime time.Time `json:"start_time"`

	// TripId uuid. Only for UPDATE
	TripId *string `json:"trip_id,omitempty"`
}

// TripPassenger defines model for TripPassenger.
type TripPassenger struct {
	Approved  bool      `json:"approved"`
	EndPlace  Place     `json:"end_place"`
	Passenger Passenger `json:"passenger"`

	// Price в копейках
	Price *int `json:"price,omitempty"`

	// SeatNumber номер места в рассадке. Нумерация начинается с 0
	SeatNumber *int `json:"seat_number,omitempty"`
}

// PostCarCreateJSONRequestBody defines body for PostCarCreate for application/json ContentType.
type PostCarCreateJSONRequestBody = CreateCarRequest

// DeleteCarDeleteJSONRequestBody defines body for DeleteCarDelete for application/json ContentType.
type DeleteCarDeleteJSONRequestBody = CarDeleteRequest

// PutCarUpdateJSONRequestBody defines body for PutCarUpdate for application/json ContentType.
type PutCarUpdateJSONRequestBody = CarUpdateRequest

// PostManagerAuthCodeJSONRequestBody defines body for PostManagerAuthCode for application/json ContentType.
type PostManagerAuthCodeJSONRequestBody = ManagerAuthCodeRequest

// PostManagerAuthInitJSONRequestBody defines body for PostManagerAuthInit for application/json ContentType.
type PostManagerAuthInitJSONRequestBody = ManagerAuthInitRequest

// PostManagerAuthRefreshTokenJSONRequestBody defines body for PostManagerAuthRefreshToken for application/json ContentType.
type PostManagerAuthRefreshTokenJSONRequestBody = RefreshToken

// PostPassengerAuthCodeJSONRequestBody defines body for PostPassengerAuthCode for application/json ContentType.
type PostPassengerAuthCodeJSONRequestBody = PassengerAuthCodeRequest

// PostPassengerAuthInitJSONRequestBody defines body for PostPassengerAuthInit for application/json ContentType.
type PostPassengerAuthInitJSONRequestBody = PassengerAuthInitRequest

// PostPassengerAuthRefreshTokenJSONRequestBody defines body for PostPassengerAuthRefreshToken for application/json ContentType.
type PostPassengerAuthRefreshTokenJSONRequestBody = RefreshToken

// PostPassengerTripBookJSONRequestBody defines body for PostPassengerTripBook for application/json ContentType.
type PostPassengerTripBookJSONRequestBody = PassengerTripBookRequest

// PostPassengerTripFilterJSONRequestBody defines body for PostPassengerTripFilter for application/json ContentType.
type PostPassengerTripFilterJSONRequestBody = PassengerTripFilterRequest

// PostPlaceCreateJSONRequestBody defines body for PostPlaceCreate for application/json ContentType.
type PostPlaceCreateJSONRequestBody = PlaceCreateRequest

// PostRouteCreateJSONRequestBody defines body for PostRouteCreate for application/json ContentType.
type PostRouteCreateJSONRequestBody = RouteCreateRequest

// DeleteRouteDeleteJSONRequestBody defines body for DeleteRouteDelete for application/json ContentType.
type DeleteRouteDeleteJSONRequestBody = RouteDeleteRequest

// PostRouteFilterJSONRequestBody defines body for PostRouteFilter for application/json ContentType.
type PostRouteFilterJSONRequestBody = RouteFilterRequest

// PutRouteUpdateJSONRequestBody defines body for PutRouteUpdate for application/json ContentType.
type PutRouteUpdateJSONRequestBody = RouteUpdateRequest

// PostTripCreateJSONRequestBody defines body for PostTripCreate for application/json ContentType.
type PostTripCreateJSONRequestBody = TripCreateRequest

// PutTripUpdateJSONRequestBody defines body for PutTripUpdate for application/json ContentType.
type PutTripUpdateJSONRequestBody = TripCreateRequest
