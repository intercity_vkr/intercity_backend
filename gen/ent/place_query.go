// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"database/sql/driver"
	"fmt"
	"intercity_backend/gen/ent/place"
	"intercity_backend/gen/ent/predicate"
	"intercity_backend/gen/ent/route"
	"intercity_backend/gen/ent/routestop"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// PlaceQuery is the builder for querying Place entities.
type PlaceQuery struct {
	config
	ctx             *QueryContext
	order           []place.OrderOption
	inters          []Interceptor
	predicates      []predicate.Place
	withRoutesStart *RouteQuery
	withRoutesEnd   *RouteQuery
	withStops       *RouteQuery
	withRouteStops  *RouteStopQuery
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the PlaceQuery builder.
func (pq *PlaceQuery) Where(ps ...predicate.Place) *PlaceQuery {
	pq.predicates = append(pq.predicates, ps...)
	return pq
}

// Limit the number of records to be returned by this query.
func (pq *PlaceQuery) Limit(limit int) *PlaceQuery {
	pq.ctx.Limit = &limit
	return pq
}

// Offset to start from.
func (pq *PlaceQuery) Offset(offset int) *PlaceQuery {
	pq.ctx.Offset = &offset
	return pq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (pq *PlaceQuery) Unique(unique bool) *PlaceQuery {
	pq.ctx.Unique = &unique
	return pq
}

// Order specifies how the records should be ordered.
func (pq *PlaceQuery) Order(o ...place.OrderOption) *PlaceQuery {
	pq.order = append(pq.order, o...)
	return pq
}

// QueryRoutesStart chains the current query on the "routes_start" edge.
func (pq *PlaceQuery) QueryRoutesStart() *RouteQuery {
	query := (&RouteClient{config: pq.config}).Query()
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := pq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := pq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(place.Table, place.FieldID, selector),
			sqlgraph.To(route.Table, route.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, false, place.RoutesStartTable, place.RoutesStartColumn),
		)
		fromU = sqlgraph.SetNeighbors(pq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// QueryRoutesEnd chains the current query on the "routes_end" edge.
func (pq *PlaceQuery) QueryRoutesEnd() *RouteQuery {
	query := (&RouteClient{config: pq.config}).Query()
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := pq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := pq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(place.Table, place.FieldID, selector),
			sqlgraph.To(route.Table, route.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, false, place.RoutesEndTable, place.RoutesEndColumn),
		)
		fromU = sqlgraph.SetNeighbors(pq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// QueryStops chains the current query on the "stops" edge.
func (pq *PlaceQuery) QueryStops() *RouteQuery {
	query := (&RouteClient{config: pq.config}).Query()
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := pq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := pq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(place.Table, place.FieldID, selector),
			sqlgraph.To(route.Table, route.FieldID),
			sqlgraph.Edge(sqlgraph.M2M, true, place.StopsTable, place.StopsPrimaryKey...),
		)
		fromU = sqlgraph.SetNeighbors(pq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// QueryRouteStops chains the current query on the "route_stops" edge.
func (pq *PlaceQuery) QueryRouteStops() *RouteStopQuery {
	query := (&RouteStopClient{config: pq.config}).Query()
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := pq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := pq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(place.Table, place.FieldID, selector),
			sqlgraph.To(routestop.Table, routestop.FieldID),
			sqlgraph.Edge(sqlgraph.O2M, true, place.RouteStopsTable, place.RouteStopsColumn),
		)
		fromU = sqlgraph.SetNeighbors(pq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first Place entity from the query.
// Returns a *NotFoundError when no Place was found.
func (pq *PlaceQuery) First(ctx context.Context) (*Place, error) {
	nodes, err := pq.Limit(1).All(setContextOp(ctx, pq.ctx, "First"))
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{place.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (pq *PlaceQuery) FirstX(ctx context.Context) *Place {
	node, err := pq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first Place ID from the query.
// Returns a *NotFoundError when no Place ID was found.
func (pq *PlaceQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = pq.Limit(1).IDs(setContextOp(ctx, pq.ctx, "FirstID")); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{place.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (pq *PlaceQuery) FirstIDX(ctx context.Context) int {
	id, err := pq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single Place entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when more than one Place entity is found.
// Returns a *NotFoundError when no Place entities are found.
func (pq *PlaceQuery) Only(ctx context.Context) (*Place, error) {
	nodes, err := pq.Limit(2).All(setContextOp(ctx, pq.ctx, "Only"))
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{place.Label}
	default:
		return nil, &NotSingularError{place.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (pq *PlaceQuery) OnlyX(ctx context.Context) *Place {
	node, err := pq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only Place ID in the query.
// Returns a *NotSingularError when more than one Place ID is found.
// Returns a *NotFoundError when no entities are found.
func (pq *PlaceQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = pq.Limit(2).IDs(setContextOp(ctx, pq.ctx, "OnlyID")); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{place.Label}
	default:
		err = &NotSingularError{place.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (pq *PlaceQuery) OnlyIDX(ctx context.Context) int {
	id, err := pq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of Places.
func (pq *PlaceQuery) All(ctx context.Context) ([]*Place, error) {
	ctx = setContextOp(ctx, pq.ctx, "All")
	if err := pq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	qr := querierAll[[]*Place, *PlaceQuery]()
	return withInterceptors[[]*Place](ctx, pq, qr, pq.inters)
}

// AllX is like All, but panics if an error occurs.
func (pq *PlaceQuery) AllX(ctx context.Context) []*Place {
	nodes, err := pq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of Place IDs.
func (pq *PlaceQuery) IDs(ctx context.Context) (ids []int, err error) {
	if pq.ctx.Unique == nil && pq.path != nil {
		pq.Unique(true)
	}
	ctx = setContextOp(ctx, pq.ctx, "IDs")
	if err = pq.Select(place.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (pq *PlaceQuery) IDsX(ctx context.Context) []int {
	ids, err := pq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (pq *PlaceQuery) Count(ctx context.Context) (int, error) {
	ctx = setContextOp(ctx, pq.ctx, "Count")
	if err := pq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return withInterceptors[int](ctx, pq, querierCount[*PlaceQuery](), pq.inters)
}

// CountX is like Count, but panics if an error occurs.
func (pq *PlaceQuery) CountX(ctx context.Context) int {
	count, err := pq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (pq *PlaceQuery) Exist(ctx context.Context) (bool, error) {
	ctx = setContextOp(ctx, pq.ctx, "Exist")
	switch _, err := pq.FirstID(ctx); {
	case IsNotFound(err):
		return false, nil
	case err != nil:
		return false, fmt.Errorf("ent: check existence: %w", err)
	default:
		return true, nil
	}
}

// ExistX is like Exist, but panics if an error occurs.
func (pq *PlaceQuery) ExistX(ctx context.Context) bool {
	exist, err := pq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the PlaceQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (pq *PlaceQuery) Clone() *PlaceQuery {
	if pq == nil {
		return nil
	}
	return &PlaceQuery{
		config:          pq.config,
		ctx:             pq.ctx.Clone(),
		order:           append([]place.OrderOption{}, pq.order...),
		inters:          append([]Interceptor{}, pq.inters...),
		predicates:      append([]predicate.Place{}, pq.predicates...),
		withRoutesStart: pq.withRoutesStart.Clone(),
		withRoutesEnd:   pq.withRoutesEnd.Clone(),
		withStops:       pq.withStops.Clone(),
		withRouteStops:  pq.withRouteStops.Clone(),
		// clone intermediate query.
		sql:  pq.sql.Clone(),
		path: pq.path,
	}
}

// WithRoutesStart tells the query-builder to eager-load the nodes that are connected to
// the "routes_start" edge. The optional arguments are used to configure the query builder of the edge.
func (pq *PlaceQuery) WithRoutesStart(opts ...func(*RouteQuery)) *PlaceQuery {
	query := (&RouteClient{config: pq.config}).Query()
	for _, opt := range opts {
		opt(query)
	}
	pq.withRoutesStart = query
	return pq
}

// WithRoutesEnd tells the query-builder to eager-load the nodes that are connected to
// the "routes_end" edge. The optional arguments are used to configure the query builder of the edge.
func (pq *PlaceQuery) WithRoutesEnd(opts ...func(*RouteQuery)) *PlaceQuery {
	query := (&RouteClient{config: pq.config}).Query()
	for _, opt := range opts {
		opt(query)
	}
	pq.withRoutesEnd = query
	return pq
}

// WithStops tells the query-builder to eager-load the nodes that are connected to
// the "stops" edge. The optional arguments are used to configure the query builder of the edge.
func (pq *PlaceQuery) WithStops(opts ...func(*RouteQuery)) *PlaceQuery {
	query := (&RouteClient{config: pq.config}).Query()
	for _, opt := range opts {
		opt(query)
	}
	pq.withStops = query
	return pq
}

// WithRouteStops tells the query-builder to eager-load the nodes that are connected to
// the "route_stops" edge. The optional arguments are used to configure the query builder of the edge.
func (pq *PlaceQuery) WithRouteStops(opts ...func(*RouteStopQuery)) *PlaceQuery {
	query := (&RouteStopClient{config: pq.config}).Query()
	for _, opt := range opts {
		opt(query)
	}
	pq.withRouteStops = query
	return pq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		ExternalID uuid.UUID `json:"external_id,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.Place.Query().
//		GroupBy(place.FieldExternalID).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
func (pq *PlaceQuery) GroupBy(field string, fields ...string) *PlaceGroupBy {
	pq.ctx.Fields = append([]string{field}, fields...)
	grbuild := &PlaceGroupBy{build: pq}
	grbuild.flds = &pq.ctx.Fields
	grbuild.label = place.Label
	grbuild.scan = grbuild.Scan
	return grbuild
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		ExternalID uuid.UUID `json:"external_id,omitempty"`
//	}
//
//	client.Place.Query().
//		Select(place.FieldExternalID).
//		Scan(ctx, &v)
func (pq *PlaceQuery) Select(fields ...string) *PlaceSelect {
	pq.ctx.Fields = append(pq.ctx.Fields, fields...)
	sbuild := &PlaceSelect{PlaceQuery: pq}
	sbuild.label = place.Label
	sbuild.flds, sbuild.scan = &pq.ctx.Fields, sbuild.Scan
	return sbuild
}

// Aggregate returns a PlaceSelect configured with the given aggregations.
func (pq *PlaceQuery) Aggregate(fns ...AggregateFunc) *PlaceSelect {
	return pq.Select().Aggregate(fns...)
}

func (pq *PlaceQuery) prepareQuery(ctx context.Context) error {
	for _, inter := range pq.inters {
		if inter == nil {
			return fmt.Errorf("ent: uninitialized interceptor (forgotten import ent/runtime?)")
		}
		if trv, ok := inter.(Traverser); ok {
			if err := trv.Traverse(ctx, pq); err != nil {
				return err
			}
		}
	}
	for _, f := range pq.ctx.Fields {
		if !place.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if pq.path != nil {
		prev, err := pq.path(ctx)
		if err != nil {
			return err
		}
		pq.sql = prev
	}
	return nil
}

func (pq *PlaceQuery) sqlAll(ctx context.Context, hooks ...queryHook) ([]*Place, error) {
	var (
		nodes       = []*Place{}
		_spec       = pq.querySpec()
		loadedTypes = [4]bool{
			pq.withRoutesStart != nil,
			pq.withRoutesEnd != nil,
			pq.withStops != nil,
			pq.withRouteStops != nil,
		}
	)
	_spec.ScanValues = func(columns []string) ([]any, error) {
		return (*Place).scanValues(nil, columns)
	}
	_spec.Assign = func(columns []string, values []any) error {
		node := &Place{config: pq.config}
		nodes = append(nodes, node)
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	for i := range hooks {
		hooks[i](ctx, _spec)
	}
	if err := sqlgraph.QueryNodes(ctx, pq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}
	if query := pq.withRoutesStart; query != nil {
		if err := pq.loadRoutesStart(ctx, query, nodes,
			func(n *Place) { n.Edges.RoutesStart = []*Route{} },
			func(n *Place, e *Route) { n.Edges.RoutesStart = append(n.Edges.RoutesStart, e) }); err != nil {
			return nil, err
		}
	}
	if query := pq.withRoutesEnd; query != nil {
		if err := pq.loadRoutesEnd(ctx, query, nodes,
			func(n *Place) { n.Edges.RoutesEnd = []*Route{} },
			func(n *Place, e *Route) { n.Edges.RoutesEnd = append(n.Edges.RoutesEnd, e) }); err != nil {
			return nil, err
		}
	}
	if query := pq.withStops; query != nil {
		if err := pq.loadStops(ctx, query, nodes,
			func(n *Place) { n.Edges.Stops = []*Route{} },
			func(n *Place, e *Route) { n.Edges.Stops = append(n.Edges.Stops, e) }); err != nil {
			return nil, err
		}
	}
	if query := pq.withRouteStops; query != nil {
		if err := pq.loadRouteStops(ctx, query, nodes,
			func(n *Place) { n.Edges.RouteStops = []*RouteStop{} },
			func(n *Place, e *RouteStop) { n.Edges.RouteStops = append(n.Edges.RouteStops, e) }); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

func (pq *PlaceQuery) loadRoutesStart(ctx context.Context, query *RouteQuery, nodes []*Place, init func(*Place), assign func(*Place, *Route)) error {
	fks := make([]driver.Value, 0, len(nodes))
	nodeids := make(map[int]*Place)
	for i := range nodes {
		fks = append(fks, nodes[i].ID)
		nodeids[nodes[i].ID] = nodes[i]
		if init != nil {
			init(nodes[i])
		}
	}
	if len(query.ctx.Fields) > 0 {
		query.ctx.AppendFieldOnce(route.FieldStartPlaceID)
	}
	query.Where(predicate.Route(func(s *sql.Selector) {
		s.Where(sql.InValues(s.C(place.RoutesStartColumn), fks...))
	}))
	neighbors, err := query.All(ctx)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		fk := n.StartPlaceID
		node, ok := nodeids[fk]
		if !ok {
			return fmt.Errorf(`unexpected referenced foreign-key "start_place_id" returned %v for node %v`, fk, n.ID)
		}
		assign(node, n)
	}
	return nil
}
func (pq *PlaceQuery) loadRoutesEnd(ctx context.Context, query *RouteQuery, nodes []*Place, init func(*Place), assign func(*Place, *Route)) error {
	fks := make([]driver.Value, 0, len(nodes))
	nodeids := make(map[int]*Place)
	for i := range nodes {
		fks = append(fks, nodes[i].ID)
		nodeids[nodes[i].ID] = nodes[i]
		if init != nil {
			init(nodes[i])
		}
	}
	if len(query.ctx.Fields) > 0 {
		query.ctx.AppendFieldOnce(route.FieldEndPlaceID)
	}
	query.Where(predicate.Route(func(s *sql.Selector) {
		s.Where(sql.InValues(s.C(place.RoutesEndColumn), fks...))
	}))
	neighbors, err := query.All(ctx)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		fk := n.EndPlaceID
		node, ok := nodeids[fk]
		if !ok {
			return fmt.Errorf(`unexpected referenced foreign-key "end_place_id" returned %v for node %v`, fk, n.ID)
		}
		assign(node, n)
	}
	return nil
}
func (pq *PlaceQuery) loadStops(ctx context.Context, query *RouteQuery, nodes []*Place, init func(*Place), assign func(*Place, *Route)) error {
	edgeIDs := make([]driver.Value, len(nodes))
	byID := make(map[int]*Place)
	nids := make(map[int]map[*Place]struct{})
	for i, node := range nodes {
		edgeIDs[i] = node.ID
		byID[node.ID] = node
		if init != nil {
			init(node)
		}
	}
	query.Where(func(s *sql.Selector) {
		joinT := sql.Table(place.StopsTable)
		s.Join(joinT).On(s.C(route.FieldID), joinT.C(place.StopsPrimaryKey[0]))
		s.Where(sql.InValues(joinT.C(place.StopsPrimaryKey[1]), edgeIDs...))
		columns := s.SelectedColumns()
		s.Select(joinT.C(place.StopsPrimaryKey[1]))
		s.AppendSelect(columns...)
		s.SetDistinct(false)
	})
	if err := query.prepareQuery(ctx); err != nil {
		return err
	}
	qr := QuerierFunc(func(ctx context.Context, q Query) (Value, error) {
		return query.sqlAll(ctx, func(_ context.Context, spec *sqlgraph.QuerySpec) {
			assign := spec.Assign
			values := spec.ScanValues
			spec.ScanValues = func(columns []string) ([]any, error) {
				values, err := values(columns[1:])
				if err != nil {
					return nil, err
				}
				return append([]any{new(sql.NullInt64)}, values...), nil
			}
			spec.Assign = func(columns []string, values []any) error {
				outValue := int(values[0].(*sql.NullInt64).Int64)
				inValue := int(values[1].(*sql.NullInt64).Int64)
				if nids[inValue] == nil {
					nids[inValue] = map[*Place]struct{}{byID[outValue]: {}}
					return assign(columns[1:], values[1:])
				}
				nids[inValue][byID[outValue]] = struct{}{}
				return nil
			}
		})
	})
	neighbors, err := withInterceptors[[]*Route](ctx, query, qr, query.inters)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		nodes, ok := nids[n.ID]
		if !ok {
			return fmt.Errorf(`unexpected "stops" node returned %v`, n.ID)
		}
		for kn := range nodes {
			assign(kn, n)
		}
	}
	return nil
}
func (pq *PlaceQuery) loadRouteStops(ctx context.Context, query *RouteStopQuery, nodes []*Place, init func(*Place), assign func(*Place, *RouteStop)) error {
	fks := make([]driver.Value, 0, len(nodes))
	nodeids := make(map[int]*Place)
	for i := range nodes {
		fks = append(fks, nodes[i].ID)
		nodeids[nodes[i].ID] = nodes[i]
		if init != nil {
			init(nodes[i])
		}
	}
	if len(query.ctx.Fields) > 0 {
		query.ctx.AppendFieldOnce(routestop.FieldPlaceID)
	}
	query.Where(predicate.RouteStop(func(s *sql.Selector) {
		s.Where(sql.InValues(s.C(place.RouteStopsColumn), fks...))
	}))
	neighbors, err := query.All(ctx)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		fk := n.PlaceID
		node, ok := nodeids[fk]
		if !ok {
			return fmt.Errorf(`unexpected referenced foreign-key "place_id" returned %v for node %v`, fk, n.ID)
		}
		assign(node, n)
	}
	return nil
}

func (pq *PlaceQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := pq.querySpec()
	_spec.Node.Columns = pq.ctx.Fields
	if len(pq.ctx.Fields) > 0 {
		_spec.Unique = pq.ctx.Unique != nil && *pq.ctx.Unique
	}
	return sqlgraph.CountNodes(ctx, pq.driver, _spec)
}

func (pq *PlaceQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := sqlgraph.NewQuerySpec(place.Table, place.Columns, sqlgraph.NewFieldSpec(place.FieldID, field.TypeInt))
	_spec.From = pq.sql
	if unique := pq.ctx.Unique; unique != nil {
		_spec.Unique = *unique
	} else if pq.path != nil {
		_spec.Unique = true
	}
	if fields := pq.ctx.Fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, place.FieldID)
		for i := range fields {
			if fields[i] != place.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := pq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := pq.ctx.Limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := pq.ctx.Offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := pq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (pq *PlaceQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(pq.driver.Dialect())
	t1 := builder.Table(place.Table)
	columns := pq.ctx.Fields
	if len(columns) == 0 {
		columns = place.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if pq.sql != nil {
		selector = pq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if pq.ctx.Unique != nil && *pq.ctx.Unique {
		selector.Distinct()
	}
	for _, p := range pq.predicates {
		p(selector)
	}
	for _, p := range pq.order {
		p(selector)
	}
	if offset := pq.ctx.Offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := pq.ctx.Limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// PlaceGroupBy is the group-by builder for Place entities.
type PlaceGroupBy struct {
	selector
	build *PlaceQuery
}

// Aggregate adds the given aggregation functions to the group-by query.
func (pgb *PlaceGroupBy) Aggregate(fns ...AggregateFunc) *PlaceGroupBy {
	pgb.fns = append(pgb.fns, fns...)
	return pgb
}

// Scan applies the selector query and scans the result into the given value.
func (pgb *PlaceGroupBy) Scan(ctx context.Context, v any) error {
	ctx = setContextOp(ctx, pgb.build.ctx, "GroupBy")
	if err := pgb.build.prepareQuery(ctx); err != nil {
		return err
	}
	return scanWithInterceptors[*PlaceQuery, *PlaceGroupBy](ctx, pgb.build, pgb, pgb.build.inters, v)
}

func (pgb *PlaceGroupBy) sqlScan(ctx context.Context, root *PlaceQuery, v any) error {
	selector := root.sqlQuery(ctx).Select()
	aggregation := make([]string, 0, len(pgb.fns))
	for _, fn := range pgb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(*pgb.flds)+len(pgb.fns))
		for _, f := range *pgb.flds {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	selector.GroupBy(selector.Columns(*pgb.flds...)...)
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := pgb.build.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

// PlaceSelect is the builder for selecting fields of Place entities.
type PlaceSelect struct {
	*PlaceQuery
	selector
}

// Aggregate adds the given aggregation functions to the selector query.
func (ps *PlaceSelect) Aggregate(fns ...AggregateFunc) *PlaceSelect {
	ps.fns = append(ps.fns, fns...)
	return ps
}

// Scan applies the selector query and scans the result into the given value.
func (ps *PlaceSelect) Scan(ctx context.Context, v any) error {
	ctx = setContextOp(ctx, ps.ctx, "Select")
	if err := ps.prepareQuery(ctx); err != nil {
		return err
	}
	return scanWithInterceptors[*PlaceQuery, *PlaceSelect](ctx, ps.PlaceQuery, ps, ps.inters, v)
}

func (ps *PlaceSelect) sqlScan(ctx context.Context, root *PlaceQuery, v any) error {
	selector := root.sqlQuery(ctx)
	aggregation := make([]string, 0, len(ps.fns))
	for _, fn := range ps.fns {
		aggregation = append(aggregation, fn(selector))
	}
	switch n := len(*ps.selector.flds); {
	case n == 0 && len(aggregation) > 0:
		selector.Select(aggregation...)
	case n != 0 && len(aggregation) > 0:
		selector.AppendSelect(aggregation...)
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := ps.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
