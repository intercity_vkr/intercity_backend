// Code generated by ent, DO NOT EDIT.

package trippassenger

import (
	"intercity_backend/gen/ent/predicate"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldID, id))
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldID, id))
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldID, id))
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldID, ids...))
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldID, ids...))
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldID, id))
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldID, id))
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldID, id))
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldID, id))
}

// CreatedAt applies equality check predicate on the "created_at" field. It's identical to CreatedAtEQ.
func CreatedAt(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldCreatedAt, v))
}

// UpdatedAt applies equality check predicate on the "updated_at" field. It's identical to UpdatedAtEQ.
func UpdatedAt(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldUpdatedAt, v))
}

// DeletedAt applies equality check predicate on the "deleted_at" field. It's identical to DeletedAtEQ.
func DeletedAt(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldDeletedAt, v))
}

// TripID applies equality check predicate on the "trip_id" field. It's identical to TripIDEQ.
func TripID(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldTripID, v))
}

// PassengerID applies equality check predicate on the "passenger_id" field. It's identical to PassengerIDEQ.
func PassengerID(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldPassengerID, v))
}

// SeatNumber applies equality check predicate on the "seat_number" field. It's identical to SeatNumberEQ.
func SeatNumber(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldSeatNumber, v))
}

// Price applies equality check predicate on the "price" field. It's identical to PriceEQ.
func Price(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldPrice, v))
}

// EndPlaceID applies equality check predicate on the "end_place_id" field. It's identical to EndPlaceIDEQ.
func EndPlaceID(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldEndPlaceID, v))
}

// Approved applies equality check predicate on the "approved" field. It's identical to ApprovedEQ.
func Approved(v bool) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldApproved, v))
}

// CreatedAtEQ applies the EQ predicate on the "created_at" field.
func CreatedAtEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldCreatedAt, v))
}

// CreatedAtNEQ applies the NEQ predicate on the "created_at" field.
func CreatedAtNEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldCreatedAt, v))
}

// CreatedAtIn applies the In predicate on the "created_at" field.
func CreatedAtIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldCreatedAt, vs...))
}

// CreatedAtNotIn applies the NotIn predicate on the "created_at" field.
func CreatedAtNotIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldCreatedAt, vs...))
}

// CreatedAtGT applies the GT predicate on the "created_at" field.
func CreatedAtGT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldCreatedAt, v))
}

// CreatedAtGTE applies the GTE predicate on the "created_at" field.
func CreatedAtGTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldCreatedAt, v))
}

// CreatedAtLT applies the LT predicate on the "created_at" field.
func CreatedAtLT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldCreatedAt, v))
}

// CreatedAtLTE applies the LTE predicate on the "created_at" field.
func CreatedAtLTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldCreatedAt, v))
}

// UpdatedAtEQ applies the EQ predicate on the "updated_at" field.
func UpdatedAtEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldUpdatedAt, v))
}

// UpdatedAtNEQ applies the NEQ predicate on the "updated_at" field.
func UpdatedAtNEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldUpdatedAt, v))
}

// UpdatedAtIn applies the In predicate on the "updated_at" field.
func UpdatedAtIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldUpdatedAt, vs...))
}

// UpdatedAtNotIn applies the NotIn predicate on the "updated_at" field.
func UpdatedAtNotIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldUpdatedAt, vs...))
}

// UpdatedAtGT applies the GT predicate on the "updated_at" field.
func UpdatedAtGT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldUpdatedAt, v))
}

// UpdatedAtGTE applies the GTE predicate on the "updated_at" field.
func UpdatedAtGTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldUpdatedAt, v))
}

// UpdatedAtLT applies the LT predicate on the "updated_at" field.
func UpdatedAtLT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldUpdatedAt, v))
}

// UpdatedAtLTE applies the LTE predicate on the "updated_at" field.
func UpdatedAtLTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldUpdatedAt, v))
}

// DeletedAtEQ applies the EQ predicate on the "deleted_at" field.
func DeletedAtEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldDeletedAt, v))
}

// DeletedAtNEQ applies the NEQ predicate on the "deleted_at" field.
func DeletedAtNEQ(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldDeletedAt, v))
}

// DeletedAtIn applies the In predicate on the "deleted_at" field.
func DeletedAtIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldDeletedAt, vs...))
}

// DeletedAtNotIn applies the NotIn predicate on the "deleted_at" field.
func DeletedAtNotIn(vs ...time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldDeletedAt, vs...))
}

// DeletedAtGT applies the GT predicate on the "deleted_at" field.
func DeletedAtGT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldDeletedAt, v))
}

// DeletedAtGTE applies the GTE predicate on the "deleted_at" field.
func DeletedAtGTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldDeletedAt, v))
}

// DeletedAtLT applies the LT predicate on the "deleted_at" field.
func DeletedAtLT(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldDeletedAt, v))
}

// DeletedAtLTE applies the LTE predicate on the "deleted_at" field.
func DeletedAtLTE(v time.Time) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldDeletedAt, v))
}

// DeletedAtIsNil applies the IsNil predicate on the "deleted_at" field.
func DeletedAtIsNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIsNull(FieldDeletedAt))
}

// DeletedAtNotNil applies the NotNil predicate on the "deleted_at" field.
func DeletedAtNotNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotNull(FieldDeletedAt))
}

// TripIDEQ applies the EQ predicate on the "trip_id" field.
func TripIDEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldTripID, v))
}

// TripIDNEQ applies the NEQ predicate on the "trip_id" field.
func TripIDNEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldTripID, v))
}

// TripIDIn applies the In predicate on the "trip_id" field.
func TripIDIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldTripID, vs...))
}

// TripIDNotIn applies the NotIn predicate on the "trip_id" field.
func TripIDNotIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldTripID, vs...))
}

// PassengerIDEQ applies the EQ predicate on the "passenger_id" field.
func PassengerIDEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldPassengerID, v))
}

// PassengerIDNEQ applies the NEQ predicate on the "passenger_id" field.
func PassengerIDNEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldPassengerID, v))
}

// PassengerIDIn applies the In predicate on the "passenger_id" field.
func PassengerIDIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldPassengerID, vs...))
}

// PassengerIDNotIn applies the NotIn predicate on the "passenger_id" field.
func PassengerIDNotIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldPassengerID, vs...))
}

// SeatNumberEQ applies the EQ predicate on the "seat_number" field.
func SeatNumberEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldSeatNumber, v))
}

// SeatNumberNEQ applies the NEQ predicate on the "seat_number" field.
func SeatNumberNEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldSeatNumber, v))
}

// SeatNumberIn applies the In predicate on the "seat_number" field.
func SeatNumberIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldSeatNumber, vs...))
}

// SeatNumberNotIn applies the NotIn predicate on the "seat_number" field.
func SeatNumberNotIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldSeatNumber, vs...))
}

// SeatNumberGT applies the GT predicate on the "seat_number" field.
func SeatNumberGT(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldSeatNumber, v))
}

// SeatNumberGTE applies the GTE predicate on the "seat_number" field.
func SeatNumberGTE(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldSeatNumber, v))
}

// SeatNumberLT applies the LT predicate on the "seat_number" field.
func SeatNumberLT(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldSeatNumber, v))
}

// SeatNumberLTE applies the LTE predicate on the "seat_number" field.
func SeatNumberLTE(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldSeatNumber, v))
}

// SeatNumberIsNil applies the IsNil predicate on the "seat_number" field.
func SeatNumberIsNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIsNull(FieldSeatNumber))
}

// SeatNumberNotNil applies the NotNil predicate on the "seat_number" field.
func SeatNumberNotNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotNull(FieldSeatNumber))
}

// PriceEQ applies the EQ predicate on the "price" field.
func PriceEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldPrice, v))
}

// PriceNEQ applies the NEQ predicate on the "price" field.
func PriceNEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldPrice, v))
}

// PriceIn applies the In predicate on the "price" field.
func PriceIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldPrice, vs...))
}

// PriceNotIn applies the NotIn predicate on the "price" field.
func PriceNotIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldPrice, vs...))
}

// PriceGT applies the GT predicate on the "price" field.
func PriceGT(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGT(FieldPrice, v))
}

// PriceGTE applies the GTE predicate on the "price" field.
func PriceGTE(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldGTE(FieldPrice, v))
}

// PriceLT applies the LT predicate on the "price" field.
func PriceLT(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLT(FieldPrice, v))
}

// PriceLTE applies the LTE predicate on the "price" field.
func PriceLTE(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldLTE(FieldPrice, v))
}

// PriceIsNil applies the IsNil predicate on the "price" field.
func PriceIsNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIsNull(FieldPrice))
}

// PriceNotNil applies the NotNil predicate on the "price" field.
func PriceNotNil() predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotNull(FieldPrice))
}

// EndPlaceIDEQ applies the EQ predicate on the "end_place_id" field.
func EndPlaceIDEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldEndPlaceID, v))
}

// EndPlaceIDNEQ applies the NEQ predicate on the "end_place_id" field.
func EndPlaceIDNEQ(v int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldEndPlaceID, v))
}

// EndPlaceIDIn applies the In predicate on the "end_place_id" field.
func EndPlaceIDIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldIn(FieldEndPlaceID, vs...))
}

// EndPlaceIDNotIn applies the NotIn predicate on the "end_place_id" field.
func EndPlaceIDNotIn(vs ...int) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNotIn(FieldEndPlaceID, vs...))
}

// ApprovedEQ applies the EQ predicate on the "approved" field.
func ApprovedEQ(v bool) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldEQ(FieldApproved, v))
}

// ApprovedNEQ applies the NEQ predicate on the "approved" field.
func ApprovedNEQ(v bool) predicate.TripPassenger {
	return predicate.TripPassenger(sql.FieldNEQ(FieldApproved, v))
}

// HasTrips applies the HasEdge predicate on the "trips" edge.
func HasTrips() predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, false, TripsTable, TripsColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasTripsWith applies the HasEdge predicate on the "trips" edge with a given conditions (other predicates).
func HasTripsWith(preds ...predicate.Trip) predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := newTripsStep()
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// HasPassenger applies the HasEdge predicate on the "passenger" edge.
func HasPassenger() predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, false, PassengerTable, PassengerColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasPassengerWith applies the HasEdge predicate on the "passenger" edge with a given conditions (other predicates).
func HasPassengerWith(preds ...predicate.Passenger) predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := newPassengerStep()
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// HasEndPlace applies the HasEdge predicate on the "end_place" edge.
func HasEndPlace() predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, false, EndPlaceTable, EndPlaceColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasEndPlaceWith applies the HasEdge predicate on the "end_place" edge with a given conditions (other predicates).
func HasEndPlaceWith(preds ...predicate.Place) predicate.TripPassenger {
	return predicate.TripPassenger(func(s *sql.Selector) {
		step := newEndPlaceStep()
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.TripPassenger) predicate.TripPassenger {
	return predicate.TripPassenger(sql.AndPredicates(predicates...))
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.TripPassenger) predicate.TripPassenger {
	return predicate.TripPassenger(sql.OrPredicates(predicates...))
}

// Not applies the not operator on the given predicate.
func Not(p predicate.TripPassenger) predicate.TripPassenger {
	return predicate.TripPassenger(sql.NotPredicates(p))
}
