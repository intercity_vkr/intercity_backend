// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"intercity_backend/gen/ent/predicate"
	"intercity_backend/gen/ent/trippassenger"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// TripPassengerUpdate is the builder for updating TripPassenger entities.
type TripPassengerUpdate struct {
	config
	hooks    []Hook
	mutation *TripPassengerMutation
}

// Where appends a list predicates to the TripPassengerUpdate builder.
func (tpu *TripPassengerUpdate) Where(ps ...predicate.TripPassenger) *TripPassengerUpdate {
	tpu.mutation.Where(ps...)
	return tpu
}

// SetUpdatedAt sets the "updated_at" field.
func (tpu *TripPassengerUpdate) SetUpdatedAt(t time.Time) *TripPassengerUpdate {
	tpu.mutation.SetUpdatedAt(t)
	return tpu
}

// SetDeletedAt sets the "deleted_at" field.
func (tpu *TripPassengerUpdate) SetDeletedAt(t time.Time) *TripPassengerUpdate {
	tpu.mutation.SetDeletedAt(t)
	return tpu
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (tpu *TripPassengerUpdate) SetNillableDeletedAt(t *time.Time) *TripPassengerUpdate {
	if t != nil {
		tpu.SetDeletedAt(*t)
	}
	return tpu
}

// ClearDeletedAt clears the value of the "deleted_at" field.
func (tpu *TripPassengerUpdate) ClearDeletedAt() *TripPassengerUpdate {
	tpu.mutation.ClearDeletedAt()
	return tpu
}

// SetSeatNumber sets the "seat_number" field.
func (tpu *TripPassengerUpdate) SetSeatNumber(i int) *TripPassengerUpdate {
	tpu.mutation.ResetSeatNumber()
	tpu.mutation.SetSeatNumber(i)
	return tpu
}

// SetNillableSeatNumber sets the "seat_number" field if the given value is not nil.
func (tpu *TripPassengerUpdate) SetNillableSeatNumber(i *int) *TripPassengerUpdate {
	if i != nil {
		tpu.SetSeatNumber(*i)
	}
	return tpu
}

// AddSeatNumber adds i to the "seat_number" field.
func (tpu *TripPassengerUpdate) AddSeatNumber(i int) *TripPassengerUpdate {
	tpu.mutation.AddSeatNumber(i)
	return tpu
}

// ClearSeatNumber clears the value of the "seat_number" field.
func (tpu *TripPassengerUpdate) ClearSeatNumber() *TripPassengerUpdate {
	tpu.mutation.ClearSeatNumber()
	return tpu
}

// SetPrice sets the "price" field.
func (tpu *TripPassengerUpdate) SetPrice(i int) *TripPassengerUpdate {
	tpu.mutation.ResetPrice()
	tpu.mutation.SetPrice(i)
	return tpu
}

// SetNillablePrice sets the "price" field if the given value is not nil.
func (tpu *TripPassengerUpdate) SetNillablePrice(i *int) *TripPassengerUpdate {
	if i != nil {
		tpu.SetPrice(*i)
	}
	return tpu
}

// AddPrice adds i to the "price" field.
func (tpu *TripPassengerUpdate) AddPrice(i int) *TripPassengerUpdate {
	tpu.mutation.AddPrice(i)
	return tpu
}

// ClearPrice clears the value of the "price" field.
func (tpu *TripPassengerUpdate) ClearPrice() *TripPassengerUpdate {
	tpu.mutation.ClearPrice()
	return tpu
}

// SetApproved sets the "approved" field.
func (tpu *TripPassengerUpdate) SetApproved(b bool) *TripPassengerUpdate {
	tpu.mutation.SetApproved(b)
	return tpu
}

// SetNillableApproved sets the "approved" field if the given value is not nil.
func (tpu *TripPassengerUpdate) SetNillableApproved(b *bool) *TripPassengerUpdate {
	if b != nil {
		tpu.SetApproved(*b)
	}
	return tpu
}

// Mutation returns the TripPassengerMutation object of the builder.
func (tpu *TripPassengerUpdate) Mutation() *TripPassengerMutation {
	return tpu.mutation
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (tpu *TripPassengerUpdate) Save(ctx context.Context) (int, error) {
	tpu.defaults()
	return withHooks(ctx, tpu.sqlSave, tpu.mutation, tpu.hooks)
}

// SaveX is like Save, but panics if an error occurs.
func (tpu *TripPassengerUpdate) SaveX(ctx context.Context) int {
	affected, err := tpu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (tpu *TripPassengerUpdate) Exec(ctx context.Context) error {
	_, err := tpu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (tpu *TripPassengerUpdate) ExecX(ctx context.Context) {
	if err := tpu.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (tpu *TripPassengerUpdate) defaults() {
	if _, ok := tpu.mutation.UpdatedAt(); !ok {
		v := trippassenger.UpdateDefaultUpdatedAt()
		tpu.mutation.SetUpdatedAt(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (tpu *TripPassengerUpdate) check() error {
	if _, ok := tpu.mutation.TripsID(); tpu.mutation.TripsCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.trips"`)
	}
	if _, ok := tpu.mutation.PassengerID(); tpu.mutation.PassengerCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.passenger"`)
	}
	if _, ok := tpu.mutation.EndPlaceID(); tpu.mutation.EndPlaceCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.end_place"`)
	}
	return nil
}

func (tpu *TripPassengerUpdate) sqlSave(ctx context.Context) (n int, err error) {
	if err := tpu.check(); err != nil {
		return n, err
	}
	_spec := sqlgraph.NewUpdateSpec(trippassenger.Table, trippassenger.Columns, sqlgraph.NewFieldSpec(trippassenger.FieldID, field.TypeInt))
	if ps := tpu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := tpu.mutation.UpdatedAt(); ok {
		_spec.SetField(trippassenger.FieldUpdatedAt, field.TypeTime, value)
	}
	if value, ok := tpu.mutation.DeletedAt(); ok {
		_spec.SetField(trippassenger.FieldDeletedAt, field.TypeTime, value)
	}
	if tpu.mutation.DeletedAtCleared() {
		_spec.ClearField(trippassenger.FieldDeletedAt, field.TypeTime)
	}
	if value, ok := tpu.mutation.SeatNumber(); ok {
		_spec.SetField(trippassenger.FieldSeatNumber, field.TypeInt, value)
	}
	if value, ok := tpu.mutation.AddedSeatNumber(); ok {
		_spec.AddField(trippassenger.FieldSeatNumber, field.TypeInt, value)
	}
	if tpu.mutation.SeatNumberCleared() {
		_spec.ClearField(trippassenger.FieldSeatNumber, field.TypeInt)
	}
	if value, ok := tpu.mutation.Price(); ok {
		_spec.SetField(trippassenger.FieldPrice, field.TypeInt, value)
	}
	if value, ok := tpu.mutation.AddedPrice(); ok {
		_spec.AddField(trippassenger.FieldPrice, field.TypeInt, value)
	}
	if tpu.mutation.PriceCleared() {
		_spec.ClearField(trippassenger.FieldPrice, field.TypeInt)
	}
	if value, ok := tpu.mutation.Approved(); ok {
		_spec.SetField(trippassenger.FieldApproved, field.TypeBool, value)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, tpu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{trippassenger.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	tpu.mutation.done = true
	return n, nil
}

// TripPassengerUpdateOne is the builder for updating a single TripPassenger entity.
type TripPassengerUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *TripPassengerMutation
}

// SetUpdatedAt sets the "updated_at" field.
func (tpuo *TripPassengerUpdateOne) SetUpdatedAt(t time.Time) *TripPassengerUpdateOne {
	tpuo.mutation.SetUpdatedAt(t)
	return tpuo
}

// SetDeletedAt sets the "deleted_at" field.
func (tpuo *TripPassengerUpdateOne) SetDeletedAt(t time.Time) *TripPassengerUpdateOne {
	tpuo.mutation.SetDeletedAt(t)
	return tpuo
}

// SetNillableDeletedAt sets the "deleted_at" field if the given value is not nil.
func (tpuo *TripPassengerUpdateOne) SetNillableDeletedAt(t *time.Time) *TripPassengerUpdateOne {
	if t != nil {
		tpuo.SetDeletedAt(*t)
	}
	return tpuo
}

// ClearDeletedAt clears the value of the "deleted_at" field.
func (tpuo *TripPassengerUpdateOne) ClearDeletedAt() *TripPassengerUpdateOne {
	tpuo.mutation.ClearDeletedAt()
	return tpuo
}

// SetSeatNumber sets the "seat_number" field.
func (tpuo *TripPassengerUpdateOne) SetSeatNumber(i int) *TripPassengerUpdateOne {
	tpuo.mutation.ResetSeatNumber()
	tpuo.mutation.SetSeatNumber(i)
	return tpuo
}

// SetNillableSeatNumber sets the "seat_number" field if the given value is not nil.
func (tpuo *TripPassengerUpdateOne) SetNillableSeatNumber(i *int) *TripPassengerUpdateOne {
	if i != nil {
		tpuo.SetSeatNumber(*i)
	}
	return tpuo
}

// AddSeatNumber adds i to the "seat_number" field.
func (tpuo *TripPassengerUpdateOne) AddSeatNumber(i int) *TripPassengerUpdateOne {
	tpuo.mutation.AddSeatNumber(i)
	return tpuo
}

// ClearSeatNumber clears the value of the "seat_number" field.
func (tpuo *TripPassengerUpdateOne) ClearSeatNumber() *TripPassengerUpdateOne {
	tpuo.mutation.ClearSeatNumber()
	return tpuo
}

// SetPrice sets the "price" field.
func (tpuo *TripPassengerUpdateOne) SetPrice(i int) *TripPassengerUpdateOne {
	tpuo.mutation.ResetPrice()
	tpuo.mutation.SetPrice(i)
	return tpuo
}

// SetNillablePrice sets the "price" field if the given value is not nil.
func (tpuo *TripPassengerUpdateOne) SetNillablePrice(i *int) *TripPassengerUpdateOne {
	if i != nil {
		tpuo.SetPrice(*i)
	}
	return tpuo
}

// AddPrice adds i to the "price" field.
func (tpuo *TripPassengerUpdateOne) AddPrice(i int) *TripPassengerUpdateOne {
	tpuo.mutation.AddPrice(i)
	return tpuo
}

// ClearPrice clears the value of the "price" field.
func (tpuo *TripPassengerUpdateOne) ClearPrice() *TripPassengerUpdateOne {
	tpuo.mutation.ClearPrice()
	return tpuo
}

// SetApproved sets the "approved" field.
func (tpuo *TripPassengerUpdateOne) SetApproved(b bool) *TripPassengerUpdateOne {
	tpuo.mutation.SetApproved(b)
	return tpuo
}

// SetNillableApproved sets the "approved" field if the given value is not nil.
func (tpuo *TripPassengerUpdateOne) SetNillableApproved(b *bool) *TripPassengerUpdateOne {
	if b != nil {
		tpuo.SetApproved(*b)
	}
	return tpuo
}

// Mutation returns the TripPassengerMutation object of the builder.
func (tpuo *TripPassengerUpdateOne) Mutation() *TripPassengerMutation {
	return tpuo.mutation
}

// Where appends a list predicates to the TripPassengerUpdate builder.
func (tpuo *TripPassengerUpdateOne) Where(ps ...predicate.TripPassenger) *TripPassengerUpdateOne {
	tpuo.mutation.Where(ps...)
	return tpuo
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (tpuo *TripPassengerUpdateOne) Select(field string, fields ...string) *TripPassengerUpdateOne {
	tpuo.fields = append([]string{field}, fields...)
	return tpuo
}

// Save executes the query and returns the updated TripPassenger entity.
func (tpuo *TripPassengerUpdateOne) Save(ctx context.Context) (*TripPassenger, error) {
	tpuo.defaults()
	return withHooks(ctx, tpuo.sqlSave, tpuo.mutation, tpuo.hooks)
}

// SaveX is like Save, but panics if an error occurs.
func (tpuo *TripPassengerUpdateOne) SaveX(ctx context.Context) *TripPassenger {
	node, err := tpuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (tpuo *TripPassengerUpdateOne) Exec(ctx context.Context) error {
	_, err := tpuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (tpuo *TripPassengerUpdateOne) ExecX(ctx context.Context) {
	if err := tpuo.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (tpuo *TripPassengerUpdateOne) defaults() {
	if _, ok := tpuo.mutation.UpdatedAt(); !ok {
		v := trippassenger.UpdateDefaultUpdatedAt()
		tpuo.mutation.SetUpdatedAt(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (tpuo *TripPassengerUpdateOne) check() error {
	if _, ok := tpuo.mutation.TripsID(); tpuo.mutation.TripsCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.trips"`)
	}
	if _, ok := tpuo.mutation.PassengerID(); tpuo.mutation.PassengerCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.passenger"`)
	}
	if _, ok := tpuo.mutation.EndPlaceID(); tpuo.mutation.EndPlaceCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "TripPassenger.end_place"`)
	}
	return nil
}

func (tpuo *TripPassengerUpdateOne) sqlSave(ctx context.Context) (_node *TripPassenger, err error) {
	if err := tpuo.check(); err != nil {
		return _node, err
	}
	_spec := sqlgraph.NewUpdateSpec(trippassenger.Table, trippassenger.Columns, sqlgraph.NewFieldSpec(trippassenger.FieldID, field.TypeInt))
	id, ok := tpuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "TripPassenger.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := tpuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, trippassenger.FieldID)
		for _, f := range fields {
			if !trippassenger.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != trippassenger.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := tpuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := tpuo.mutation.UpdatedAt(); ok {
		_spec.SetField(trippassenger.FieldUpdatedAt, field.TypeTime, value)
	}
	if value, ok := tpuo.mutation.DeletedAt(); ok {
		_spec.SetField(trippassenger.FieldDeletedAt, field.TypeTime, value)
	}
	if tpuo.mutation.DeletedAtCleared() {
		_spec.ClearField(trippassenger.FieldDeletedAt, field.TypeTime)
	}
	if value, ok := tpuo.mutation.SeatNumber(); ok {
		_spec.SetField(trippassenger.FieldSeatNumber, field.TypeInt, value)
	}
	if value, ok := tpuo.mutation.AddedSeatNumber(); ok {
		_spec.AddField(trippassenger.FieldSeatNumber, field.TypeInt, value)
	}
	if tpuo.mutation.SeatNumberCleared() {
		_spec.ClearField(trippassenger.FieldSeatNumber, field.TypeInt)
	}
	if value, ok := tpuo.mutation.Price(); ok {
		_spec.SetField(trippassenger.FieldPrice, field.TypeInt, value)
	}
	if value, ok := tpuo.mutation.AddedPrice(); ok {
		_spec.AddField(trippassenger.FieldPrice, field.TypeInt, value)
	}
	if tpuo.mutation.PriceCleared() {
		_spec.ClearField(trippassenger.FieldPrice, field.TypeInt)
	}
	if value, ok := tpuo.mutation.Approved(); ok {
		_spec.SetField(trippassenger.FieldApproved, field.TypeBool, value)
	}
	_node = &TripPassenger{config: tpuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, tpuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{trippassenger.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	tpuo.mutation.done = true
	return _node, nil
}
