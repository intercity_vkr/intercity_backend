package main

import (
	"context"
	"intercity_backend/internal/app"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func main() {
	ctx := context.Background()
	application := app.NewApp()
	err := application.InitApp(ctx)
	if err != nil {
		panic(err)
	}

	go StartProxy()
	err = application.Run()
	if err != nil {
		panic(err)
	}
}

func StartProxy() {
	http.HandleFunc("/", handleRequests)
	http.ListenAndServe(":9789", corsHandler(http.DefaultServeMux))

}

func handleRequests(w http.ResponseWriter, r *http.Request) {
	targetUrl, _ := url.Parse("https://api.gigachat.net/") // Адрес API Gigachat
	proxy := httputil.NewSingleHostReverseProxy(targetUrl)
	proxy.ServeHTTP(w, r)
}

func corsHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		next.ServeHTTP(w, r)
	})
}
