package sq

import (
	"github.com/Masterminds/squirrel"
	"strings"
)

var SQ = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)

func GetFiltersField(table, field string) string {
	return strings.Join([]string{table, field}, ".")
}
