package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"golang.org/x/tools/go/ast/inspector"
	"log"
	"os"
)

type GenData struct {
	NameStruct string
	fieldsTags map[string]string
}

func main() {
	path := os.Getenv("GOFILE")
	fmt.Println(path)
	tok := token.NewFileSet()
	astInFile, err := parser.ParseFile(tok, path, nil, parser.ParseComments)
	if err != nil {
		log.Fatalf(err.Error())
	}
	insp := inspector.New([]*ast.File{astInFile})
	iFilter := []ast.Node{
		&ast.GenDecl{},
	}
	genData := GenData{
		fieldsTags: make(map[string]string),
	}
	insp.Nodes(iFilter, func(n ast.Node, push bool) (proceed bool) {
		genDecl := n.(*ast.GenDecl)
		typeSpec, ok := genDecl.Specs[0].(*ast.TypeSpec)
		if !ok {
			return false
		}
		structType, ok := typeSpec.Type.(*ast.StructType)
		if !ok {
			return false
		}
		genData.NameStruct = typeSpec.Name.Name
		for _, v := range structType.Fields.List {
			if len(v.Names) == 0 {
				continue
			}
			//fmt.Printf("%s-%s\n", v.Names[0].Name, v.Tag.Value)
			genData.fieldsTags[v.Names[0].Name] = v.Tag.Value
		}
		return false
	})

	println(genData.NameStruct)
	for k, v := range genData.fieldsTags {
		println(k, "---", v)
	}
}
