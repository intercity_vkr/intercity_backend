package jwt

import "github.com/golang-jwt/jwt/v5"

func NewJwtWithClaims(key string, claims jwt.Claims) (string, error) {
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return t.SignedString([]byte(key))
}

func ParseJwt(token, key string, claims jwt.Claims) (jwt.Claims, error) {
	if claims == nil {
		return nil, jwt.ErrTokenInvalidClaims
	}
	parsedToken, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	if err != nil {
		return nil, err
	}
	if !parsedToken.Valid {
		return nil, jwt.ErrSignatureInvalid
	}
	//resp, ok := parsedToken.Claims.(T)
	//if !ok {
	//	return nil, jwt.ErrTokenInvalidClaims
	//}
	return parsedToken.Claims, nil
}
