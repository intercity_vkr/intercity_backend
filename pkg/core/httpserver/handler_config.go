package httpserver

var _ IHandlerConfig = (*HandlerConfig)(nil)

type HandlerConfig struct {
	Path            string
	HttpMethod      string
	Middlewares     *Middlewares
	RequestBodyDto  any
	PathParamsDto   any
	QueryParamsDto  any
	ResponseDto     any
	Auth            string
	CustomErrMapper ErrorMapperFunc
}

func (h *HandlerConfig) GetCustomErrorMapper() ErrorMapperFunc {
	return h.CustomErrMapper
}

func (h *HandlerConfig) GetPath() string {
	return h.Path
}

func (h *HandlerConfig) GetHttpMethod() string {
	return h.HttpMethod
}

func (h *HandlerConfig) GetMiddleware() *Middlewares {
	return h.Middlewares
}

func (h *HandlerConfig) GetRequestBodyDto() any {
	return h.RequestBodyDto
}

func (h *HandlerConfig) GetRequestPathParamsDto() any {
	return h.PathParamsDto
}

func (h *HandlerConfig) GetRequestQueryParamsDto() any {
	return h.QueryParamsDto
}

func (h *HandlerConfig) GetResponseDto() any {
	return h.ResponseDto
}

func (h *HandlerConfig) GetAuth() string {
	return h.Auth
}
