package httpserver

import (
	"context"
	"fmt"
	"net/http"
)

type ErrorMapperFunc func(ctx context.Context, err error) HttpError

type HttpError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	TraceId string `json:"trace_id"`
	Detail  string `json:"detail"`
}

func (he HttpError) Error() string {
	detail := "NO"
	if he.Detail != "" {
		detail = he.Detail
	}
	return fmt.Sprintf("code - %d message - %s detail - %s trace_id - %s", he.Code, he.Message, detail, he.TraceId)
}

func NewErrorNotFound(detail string) HttpError {
	return HttpError{
		Code:    http.StatusNotFound,
		Message: "Not Found",
		Detail:  detail,
	}
}

func NewErrorBadRequest(detail string) HttpError {
	return HttpError{
		Code:    http.StatusBadRequest,
		Message: "Bad Request",
		Detail:  detail,
	}
}

func NewErrorInternalError(detail string) HttpError {
	return HttpError{
		Code:    http.StatusInternalServerError,
		Message: "Internal Server Error",
		Detail:  detail,
	}
}

func NewErrorUnauthorized(detail string) HttpError {
	return HttpError{
		Code:    http.StatusUnauthorized,
		Message: "Unauthorized",
		Detail:  detail,
	}
}
