package httpserver

import (
	"context"
	"github.com/gin-gonic/gin"
)

type HandlerContext struct {
	GCtx  *gin.Context
	Actor IActor
}

type RequestData struct {
	BodyDto        any
	PathParamsDto  any
	QueryParamsDto any
}

type ResponseData struct {
	Status int
	Data   any
}

type Middlewares struct {
	BeforeWrappedHandler *gin.HandlersChain
	BeforeHandler        *gin.HandlersChain
	AfterHandler         *gin.HandlersChain
}

type IHandlerConfig interface {
	GetPath() string
	GetHttpMethod() string
	GetMiddleware() *Middlewares
	GetRequestBodyDto() any
	GetRequestPathParamsDto() any
	GetRequestQueryParamsDto() any
	GetResponseDto() any
	GetAuth() string
	GetCustomErrorMapper() ErrorMapperFunc
}

type HandlerFunc func(ctx context.Context, hc *HandlerContext, requestData *RequestData) (*ResponseData, error)

type IHandler interface {
	GetConfig() IHandlerConfig
	Handler() HandlerFunc
}
type IHandlers []IHandler
