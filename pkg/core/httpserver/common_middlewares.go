package httpserver

type IAuthMiddleware interface {
	Middleware(hc *HandlerContext, role string) (IActor, bool)
}

type IActorMiddleware interface {
	Middleware(hc *HandlerContext, actor string) IActor
}

type CommonMiddlewares struct {
	AuthMiddleware  IAuthMiddleware
	ActorMiddleware IActorMiddleware
}
