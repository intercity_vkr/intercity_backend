package httpserver

import (
	"context"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type HttpServer struct {
	router            *gin.Engine
	httpServer        *http.Server
	commonMiddlewares CommonMiddlewares
	errorMapperFunc   ErrorMapperFunc
}

type Config struct {
	Port int
}

func (hs *HttpServer) InitRouter() {
	hs.router = gin.Default()
}

func (hs *HttpServer) InitServer(config Config) {
	hs.httpServer = &http.Server{
		Addr:    fmt.Sprintf(":%d", config.Port),
		Handler: hs.router,
	}
}

func (hs *HttpServer) InitErrorMapper(m ErrorMapperFunc) {
	hs.errorMapperFunc = m
}

func (hs *HttpServer) ListenAndServe() error {
	return hs.httpServer.ListenAndServe()
}
func (hs *HttpServer) Shutdown(ctx context.Context) error {
	return hs.httpServer.Shutdown(ctx)
}

func (hs *HttpServer) HandlerWrapper(handler IHandler) gin.HandlerFunc {
	return func(c *gin.Context) {
		handlerConfig := handler.GetConfig()
		hc := &HandlerContext{GCtx: c}
		// authorization
		authRole := handlerConfig.GetAuth()
		if authRole != "" {
			actor, authorized := hs.commonMiddlewares.AuthMiddleware.Middleware(hc, authRole)
			if !authorized {
				hs.handleErrorWithAbort(c, handlerConfig, NewErrorUnauthorized(""))
				return
			}
			hc.Actor = actor
		}
		requestData := RequestData{
			BodyDto:        handlerConfig.GetRequestBodyDto(),
			PathParamsDto:  handlerConfig.GetRequestPathParamsDto(),
			QueryParamsDto: handlerConfig.GetRequestQueryParamsDto(),
		}
		if requestData.BodyDto != nil {
			err := c.ShouldBindJSON(requestData.BodyDto)
			if err != nil {
				hs.handleErrorWithAbort(c, handlerConfig, NewErrorBadRequest("body is not valid"))
				return
			}
		}
		if requestData.PathParamsDto != nil {
			err := c.ShouldBindUri(&requestData.PathParamsDto)
			if err != nil {
				hs.handleErrorWithAbort(c, handlerConfig, NewErrorBadRequest("uri is not valid"))
				return
			}
		}
		if requestData.QueryParamsDto != nil {
			err := c.ShouldBindQuery(requestData.QueryParamsDto)
			if err != nil {
				hs.handleErrorWithAbort(c, handlerConfig, NewErrorBadRequest("query params is not valid"))
				return
			}
		}
		resp, err := handler.Handler()(hc.GCtx.Request.Context(), hc, &requestData)
		if err != nil {
			hs.handleErrorWithAbort(c, handlerConfig, err)
		}
		if resp == nil {
			c.JSON(http.StatusOK, nil)
			return
		}
		status := http.StatusOK
		if resp.Status != 0 {
			status = resp.Status
		}
		c.JSON(status, resp.Data)
	}
}

func (hs *HttpServer) handleError(c *gin.Context, handlerConfig IHandlerConfig, err error) {
	var httpError HttpError
	if !errors.As(err, &httpError) {
		errMapper := hs.getErrorMapper(handlerConfig)
		httpError = errMapper(c, err)
	}
	c.JSON(httpError.Code, httpError)
}

func (hs *HttpServer) handleErrorWithAbort(c *gin.Context, handlerConfig IHandlerConfig, err error) {
	hs.handleError(c, handlerConfig, err)
	c.Abort()
}

func (hs *HttpServer) AddHandler(handler IHandler) {
	handlerConfig := handler.GetConfig()
	if handlerConfig == nil {
		panic(fmt.Errorf("handler without config"))
	}
	var handlers gin.HandlersChain
	handlers = append(handlers, hs.HandlerWrapper(handler))
	hs.router.Handle(handlerConfig.GetHttpMethod(), handlerConfig.GetPath(), handlers...)
}

func (hs *HttpServer) AddHandlers(handlers IHandlers) {
	for _, handler := range handlers {
		hs.AddHandler(handler)
	}
}

func (hs *HttpServer) AddCommonMiddlewares(middlewares CommonMiddlewares) {
	hs.commonMiddlewares = middlewares
}

func (hs *HttpServer) AddGlobalCorsMiddleware() {
	hs.router.Use(GetCorsMiddleware())
}

func (hs *HttpServer) getErrorMapper(handlerConfig IHandlerConfig) ErrorMapperFunc {
	custom := handlerConfig.GetCustomErrorMapper()
	if custom != nil {
		return custom
	}
	return hs.errorMapperFunc
}
