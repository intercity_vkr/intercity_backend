package httpserver

import "github.com/google/uuid"

type IActor interface {
	GetId() int
	GetExternalId() uuid.UUID
	GetActorType() string
}

func ConcreteActor[T IActor](v IActor) T {
	return v.(T)
}
