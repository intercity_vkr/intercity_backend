package db

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Database struct {
	Pool   *pgxpool.Pool
	dbUrl  string
	config Config
}

type Config struct {
	Username string
	Password string
	Host     string
	Port     string
	DbName   string
}

func NewDatabase(_ context.Context, config Config) *Database {
	return &Database{config: config}
}

func (db *Database) CreatePool(ctx context.Context) error {
	pool, err := pgxpool.New(ctx, fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		db.config.Username, db.config.Password, db.config.Host, db.config.Port, db.config.DbName))
	if err != nil {
		return err
	}
	db.Pool = pool
	return nil
}

func (db *Database) ClosePool(_ context.Context) {
	db.Pool.Close()
}

func (db *Database) Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error) {
	return db.Pool.Exec(ctx, sql, arguments...)
}

func (db *Database) Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error) {
	return db.Pool.Query(ctx, sql, args...)
}

func (db *Database) QueryRow(ctx context.Context, sql string, args ...any) pgx.Row {
	return db.Pool.QueryRow(ctx, sql, args...)
}
