package vk_map

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"intercity_backend/pkg/geo/entity"
	"testing"
)

func TestVkMapClient_DoReverseCoding(t *testing.T) {
	t.Skip()
	at := assert.New(t)
	vkMapClient := NewVkMapClient("32bdd90a32bdd90a32bdd90a3431aacd0a332bd32bdd90a570b77f5d694a04aa88eed34")
	ctx := context.Background()
	resp, err := vkMapClient.DoReverseCoding(ctx, entity.Point{
		Longitude: 56.172965,
		Latitude:  51.846776,
	}, 3000)
	at.NoError(err)
	fmt.Println(resp)
}
