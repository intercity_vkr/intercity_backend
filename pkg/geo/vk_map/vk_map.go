package vk_map

import (
	"context"
	"encoding/json"
	"fmt"
	"intercity_backend/pkg/geo"
	"intercity_backend/pkg/geo/entity"
	"intercity_backend/pkg/utils"
	"net/http"
	"net/url"
)

type VkMapClient struct {
	serviceKey string
	scheme     string
	host       string
	endpoint   string
}

func NewVkMapClient(serviceKey string) *VkMapClient {
	return &VkMapClient{
		serviceKey: serviceKey,
		scheme:     "https",
		host:       "demo.maps.vk.com",
		endpoint:   "api/search",
	}
}

func (vk *VkMapClient) DoReverseCoding(ctx context.Context, point entity.Point, radiusMeters int) (*entity.Place, error) {
	query := make(url.Values)
	query.Set("api_key", vk.serviceKey)
	query.Set("q", fmt.Sprintf("%f,%f", point.Latitude, point.Longitude))
	query.Set("lang", "ru")
	query.Set("limit", "1")
	query.Set("fields", "address_details,pin,type,address")
	query.Set("admin_level", "3")
	query.Set("radius", fmt.Sprintf("%d", radiusMeters))

	u := url.URL{
		Scheme:   vk.scheme,
		Host:     vk.host,
		Path:     vk.endpoint,
		RawQuery: query.Encode(),
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	utils.AddHeaderJson(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	defer resp.Body.Close()

	outp := struct {
		Request string `json:"request"`
		Results []struct {
			AddressDetails struct {
				Country   string `json:"country"`
				Locality  string `json:"locality"`
				Region    string `json:"region"`
				Subregion string `json:"subregion"`
			} `json:"address_details"`
			Pin  []float64 `json:"pin"`
			Type string    `json:"type"`
		} `json:"results"`
	}{}
	err = json.NewDecoder(resp.Body).Decode(&outp)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	if utils.SliceIsEmptyOrNil(outp.Results) {
		return nil, geo.ErrNotFound
	}
	return &entity.Place{
		Pin: entity.Point{
			Longitude: outp.Results[0].Pin[0],
			Latitude:  outp.Results[0].Pin[1],
		},
		Country:   outp.Results[0].AddressDetails.Country,
		Locality:  outp.Results[0].AddressDetails.Locality,
		Region:    outp.Results[0].AddressDetails.Region,
		SubRegion: outp.Results[0].AddressDetails.Subregion,
	}, nil
}
