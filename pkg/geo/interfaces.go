package geo

import (
	"context"
	"intercity_backend/pkg/geo/entity"
)

type Router interface {
	GetRoute(ctx context.Context, points entity.Points) (entity.Routes, error)
}

type ReverseCoder interface {
	DoReverseCoding(ctx context.Context, point entity.Point, radiusMeters int) (*string, error)
}
