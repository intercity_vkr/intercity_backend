package entity

type Point struct {
	Longitude float64
	Latitude  float64
}
type Points []*Point

func NewPointsFromList(l [][]float64) Points {
	p := make(Points, len(l))
	for idx, v := range l {
		p[idx] = &Point{
			Longitude: v[0],
			Latitude:  v[1],
		}
	}
	return p
}

func (p *Point) ToList() []float64 {
	return []float64{p.Longitude, p.Latitude}
}
func (p *Point) ToLatLongList() []float64 {
	return []float64{p.Latitude, p.Longitude}
}

func (p *Points) ToCommonList() []float64 {
	resp := make([]float64, len(*p)*2)
	i := 0
	for _, point := range *p {
		resp[i] = point.Longitude
		resp[i+1] = point.Latitude
		i += 2
	}
	return resp
}

func (p *Points) ToList() [][]float64 {
	resp := make([][]float64, len(*p))
	for idx, point := range *p {
		resp[idx] = point.ToList()
	}
	return resp
}
