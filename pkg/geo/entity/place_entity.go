package entity

type Place struct {
	Pin       Point  `json:"pin"`
	Country   string `json:"country"`
	Locality  string `json:"locality"`
	Region    string `json:"region"`
	SubRegion string `json:"sub_region"`
}
