package entity

type Routes []*Route

type Route struct {
	Distance    float64 `json:"distance"`
	Weight      float64 `json:"weight"`
	BBox        BBox    `json:"b_box"`
	StartPoint  Point   `json:"start_point"`
	EndPoint    Point   `json:"end_point"`
	StopPoints  Points  `json:"stop_points"`
	Coordinates Points  `json:"coordinates"`
}

type BBox struct {
	PointMin Point `json:"point_min"`
	PointMax Point `json:"point_max"`
}
