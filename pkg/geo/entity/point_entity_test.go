package entity

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPoint_ToList(t *testing.T) {
	at := assert.New(t)
	p := Point{
		Longitude: 5.05,
		Latitude:  4.04,
	}
	at.Equal([]float64{5.05, 4.04}, p.ToList())
}

func TestPoint_ToLatLongList(t *testing.T) {
	at := assert.New(t)
	p := Point{
		Longitude: 5.05,
		Latitude:  4.04,
	}
	at.Equal([]float64{4.04, 5.05}, p.ToLatLongList())
}

func TestPoints_ToCommonList(t *testing.T) {
	at := assert.New(t)
	p1 := Point{
		Longitude: 5.05,
		Latitude:  4.04,
	}
	p2 := Point{
		Longitude: 6.05,
		Latitude:  5.04,
	}
	p3 := Point{
		Longitude: 7.05,
		Latitude:  6.04,
	}
	points := Points{&p1, &p2, &p3}
	at.Equal([]float64{5.05, 4.04, 6.05, 5.04, 7.05, 6.04}, points.ToCommonList())
}

func TestPoints_ToList(t *testing.T) {
	at := assert.New(t)
	p1 := Point{
		Longitude: 5.05,
		Latitude:  4.04,
	}
	p2 := Point{
		Longitude: 6.05,
		Latitude:  5.04,
	}
	p3 := Point{
		Longitude: 7.05,
		Latitude:  6.04,
	}
	points := Points{&p1, &p2, &p3}
	at.Equal([][]float64{{5.05, 4.04}, {6.05, 5.04}, {7.05, 6.04}}, points.ToList())
}

func TestNewPointsFromList(t *testing.T) {
	at := assert.New(t)
	l := [][]float64{{5.65, 4.65}, {6.01, 7.01}, {0.25, 0.35}}
	at.Equal(Points{&Point{
		Longitude: 5.65,
		Latitude:  4.65,
	},
		&Point{
			Longitude: 6.01,
			Latitude:  7.01,
		},
		&Point{
			Longitude: 0.25,
			Latitude:  0.35,
		}}, NewPointsFromList(l))
}
