package graphhopper

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"intercity_backend/pkg/geo"
	"intercity_backend/pkg/geo/entity"
	"intercity_backend/pkg/utils"
	"net/http"
	"net/url"
	"time"
)

type GraphhopperClient struct {
	apiKey string
	scheme string
	host   string
}

func NewGraphhopperClient(apiKey string) *GraphhopperClient {
	return &GraphhopperClient{apiKey: apiKey, scheme: "https", host: "graphhopper.com"}
}

func (gc *GraphhopperClient) GetRoute(ctx context.Context, points entity.Points) (entity.Routes, error) {
	v := make(url.Values)
	v.Set("key", gc.apiKey)
	u := url.URL{
		Scheme:      gc.scheme,
		Host:        gc.host,
		Path:        "api/1/route",
		RawQuery:    v.Encode(),
		Fragment:    "",
		RawFragment: "",
	}

	body := struct {
		Profile                  string      `json:"profiles"`
		Points                   [][]float64 `json:"points"`
		Locale                   string      `json:"locale"`
		PointsEncoded            bool        `json:"points_encoded"`
		Debug                    bool        `json:"debug"`
		Algorithm                string      `json:"algorithm"`
		AlternativeRouteMaxPaths int         `json:"alternative_route.max_paths"`
		Details                  []string    `json:"details"`
		SnapPreventions          []string    `json:"snap_preventions"`
		Elevation                bool        `json:"elevation"`
		Instructions             bool        `json:"instructions"`
	}{
		Profile:                  "car",
		Locale:                   "ru",
		Points:                   points.ToList(),
		PointsEncoded:            false,
		Debug:                    true,
		Algorithm:                "alternative_route",
		AlternativeRouteMaxPaths: 1,
		Details:                  []string{},
		SnapPreventions:          []string{"ferry"},
		Elevation:                true,
		Instructions:             false,
	}
	requestBodyJson, err := json.Marshal(body)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, u.String(), bytes.NewReader(requestBodyJson))
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	utils.AddHeaderJson(req)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusBadRequest:
		return nil, utils.WrapError(geo.ErrInvalidRequest, resp.Status)
	case http.StatusUnauthorized:
		return nil, utils.WrapError(geo.ErrAuth, resp.Status)
	case http.StatusTooManyRequests:
		return nil, utils.WrapError(geo.ErrApiLimit, resp.Status)
	case http.StatusInternalServerError:
		return nil, utils.WrapError(geo.ErrServiceNotAvailable, resp.Status)
	}

	outp := struct {
		Hints struct {
			VisitedNodesSum     int     `json:"visited_nodes.sum"`
			VisitedNodesAverage float64 `json:"visited_nodes.average"`
		} `json:"hints"`
		Info struct {
			Copyrights        []string  `json:"copyrights"`
			Took              int       `json:"took"`
			RoadDataTimestamp time.Time `json:"road_data_timestamp"`
		} `json:"info"`
		Paths []struct {
			Distance      float64   `json:"distance"`
			Weight        float64   `json:"weight"`
			Time          int       `json:"time"`
			Transfers     int       `json:"transfers"`
			PointsEncoded bool      `json:"points_encoded"`
			Bbox          []float64 `json:"bbox"`
			Points        struct {
				Type        string      `json:"type"`
				Coordinates [][]float64 `json:"coordinates"`
			} `json:"points"`
			Legs    []any `json:"legs"`
			Details struct {
			} `json:"details"`
			Ascend           float64 `json:"ascend"`
			Descend          float64 `json:"descend"`
			SnappedWaypoints struct {
				Type        string      `json:"type"`
				Coordinates [][]float64 `json:"coordinates"`
			} `json:"snapped_waypoints"`
		} `json:"paths"`
	}{}
	err = json.NewDecoder(resp.Body).Decode(&outp)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}

	routes := make(entity.Routes, len(outp.Paths))
	for idx, path := range outp.Paths {
		routes[idx] = &entity.Route{
			Distance: path.Distance,
			Weight:   path.Weight,
			BBox: entity.BBox{
				PointMin: entity.Point{
					Longitude: path.Bbox[0],
					Latitude:  path.Bbox[1],
				},
				PointMax: entity.Point{
					Longitude: path.Bbox[2],
					Latitude:  path.Bbox[3],
				},
			},
			StartPoint:  *points[0],
			EndPoint:    *points[len(points)-1],
			StopPoints:  nil,
			Coordinates: entity.NewPointsFromList(path.Points.Coordinates),
		}
	}
	return routes, nil
}

func (gc *GraphhopperClient) DoReverseCoding(ctx context.Context, point entity.Point, radiusMeters int) (*string, error) {
	v := make(url.Values)
	v.Set("key", gc.apiKey)
	v.Set("locale", "ru")
	v.Set("reverse", "true")
	v.Set("point", fmt.Sprintf("%f, %f", point.Latitude, point.Longitude))
	v.Set("provider", "default")
	v.Set("radius", fmt.Sprintf("%d", radiusMeters/1000))
	u := url.URL{
		Scheme:      gc.scheme,
		Host:        gc.host,
		Path:        "api/1/geocode",
		RawQuery:    v.Encode(),
		Fragment:    "",
		RawFragment: "",
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	defer resp.Body.Close()

	outp := struct {
		Hits []struct {
			Point struct {
				Lat float64 `json:"lat"`
				Lng float64 `json:"lng"`
			} `json:"point"`
			Extent      []float64 `json:"extent"`
			Name        string    `json:"name"`
			Country     string    `json:"country"`
			Countrycode string    `json:"countrycode"`
			City        string    `json:"city"`
			State       string    `json:"state"`
			Postcode    string    `json:"postcode"`
			OsmID       int       `json:"osm_id"`
			OsmType     string    `json:"osm_type"`
			OsmKey      string    `json:"osm_key"`
			OsmValue    string    `json:"osm_value"`
		} `json:"hits"`
		Locale string `json:"locale"`
	}{}
	err = json.NewDecoder(resp.Body).Decode(&outp)
	if err != nil {
		return nil, utils.WrapError(geo.ErrInternal, err.Error())
	}
	return nil, nil
}
