package graphhopper

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"intercity_backend/pkg/geo/entity"
	"testing"
)

func TestGraphhopperClient_GetRoute(t *testing.T) {
	t.Skip()
	at := assert.New(t)
	graphhopperClient := NewGraphhopperClient("0813a1dc-0285-4541-a00c-63ca2559c174")
	ctx := context.Background()
	resp, err := graphhopperClient.GetRoute(ctx, entity.Points{
		&entity.Point{
			Longitude: 55.128811,
			Latitude:  51.833353,
		},
		&entity.Point{
			Longitude: 56.357759,
			Latitude:  51.782136,
		}})
	at.NoError(err)
	fmt.Println("SUCCESS")
	fmt.Println(resp)
}

func TestGraphhopperClient_DoReverseCoding(t *testing.T) {
	at := assert.New(t)
	graphhopperClient := NewGraphhopperClient("0813a1dc-0285-4541-a00c-63ca2559c174")
	ctx := context.Background()
	resp, err := graphhopperClient.DoReverseCoding(ctx, entity.Point{
		Longitude: 55.858491,
		Latitude:  51.861275,
	}, 30)
	at.NoError(err)
	fmt.Println("SUCCESS")
	fmt.Println(resp)
}
