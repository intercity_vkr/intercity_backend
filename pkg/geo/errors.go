package geo

import "errors"

var ErrInvalidRequest = errors.New("invalid request")
var ErrAuth = errors.New("auth is necessary ")
var ErrApiLimit = errors.New("API limit")
var ErrServiceNotAvailable = errors.New("service is not available ")
var ErrInternal = errors.New("internal server error")
var ErrNotFound = errors.New("not found")
