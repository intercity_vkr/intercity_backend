package utils

import "net/http"

func AddHeaderJson(r *http.Request) {
	r.Header.Add("Content-Type", "application/json")
}
