package utils

import (
	"fmt"
)

func WrapError(baseErr error, text string) error {
	return fmt.Errorf("[%w]: %s", baseErr, text)
}
