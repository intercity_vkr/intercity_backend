package utils

import (
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgtype"
)

func UuidGoogleToPgType(v uuid.UUID) pgtype.UUID {
	var resp pgtype.UUID
	copy(resp.Bytes[:], v[:])
	resp.Valid = true
	return resp
}
