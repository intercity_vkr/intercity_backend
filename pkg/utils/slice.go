package utils

func SliceIsEmptyOrNil[T any](s []T) bool {
	return s == nil || len(s) == 0
}
