generate-swagger:
	oapi-codegen -generate types openapi/openapi.yml > gen/openapi/intercity.gen.go

generate-dto-dao:
	sqlc -f config/sqlc.yml generate

db-migrate:
	migrate -database pgx5://intercity_user:intercity_password@localhost:5420/intercity?sslmode=disable -path db/migrations up

ent-generate:
	go run -mod=mod entgo.io/ent/cmd/ent generate --target gen/ent ./internal/dao/schema

