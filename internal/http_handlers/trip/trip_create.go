package trip

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*TripCreate)(nil)

type TripCreate struct {
	tripDao  *dao.Trip
	idMapper *dao.IdMapper
}

func NewTripCreateHandler(tripDao *dao.Trip, m *dao.IdMapper) *TripCreate {
	return &TripCreate{tripDao: tripDao, idMapper: m}
}

func (c *TripCreate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.TripCreate,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.TripCreateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Trip{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *TripCreate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.TripCreateRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}

		routeId, err := c.idMapper.GetIdByExtId(ctx, &entities.Route{}, uuid.MustParse(reqRoute.RouteId))
		if err != nil {
			return nil, err
		}

		carId, err := c.idMapper.GetIdByExtId(ctx, &entities.CarEntity{}, uuid.MustParse(reqRoute.CarId))
		if err != nil {
			return nil, err
		}
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		trip, err := c.tripDao.CreateTrip(ctx, &entities.Trip{
			CompanyId: int(actor.CompanyId),
			RouteId:   routeId,
			Reversed:  reqRoute.Reversed,
			CarId:     carId,
			StartTime: reqRoute.StartTime,
			EndTime:   reqRoute.EndTime,
			FullPrice: reqRoute.FullPrice,
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TripEntityToHttp(trip),
		}, nil
	}
}
