package trip

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*PassengerTripFilter)(nil)

type PassengerTripFilter struct {
	tripDao  *dao.Trip
	idMapper *dao.IdMapper
}

func NewPassengerTripFilterHandler(tripDao *dao.Trip, idMapper *dao.IdMapper) *PassengerTripFilter {
	return &PassengerTripFilter{tripDao: tripDao, idMapper: idMapper}
}

func (c *PassengerTripFilter) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerTripFilter,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.PassengerTripFilterRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Trip{},
		Auth:           entities.PassengerUserType,
	}
}

func (c *PassengerTripFilter) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		req, ok := requestData.BodyDto.(*openapi.PassengerTripFilterRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		startPlaceExtId, err := uuid.Parse(req.StartPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid start_place_id")
		}
		endPlaceExtId, err := uuid.Parse(req.EndPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid end_place_id")
		}
		startPlaceId, err := c.idMapper.GetIdByExtId(ctx, &entities.Place{}, startPlaceExtId)
		if err != nil {
			return nil, err
		}
		endPlaceId, err := c.idMapper.GetIdByExtId(ctx, &entities.Place{}, endPlaceExtId)
		if err != nil {
			return nil, err
		}
		trips, err := c.tripDao.SearchForPassenger(ctx, startPlaceId, endPlaceId, req.StartTime, req.EndTime)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Trip {
				res := make([]*openapi.Trip, 0, len(trips))
				for _, car := range trips {
					res = append(res, mappers.TripEntityToHttp(car))
				}
				return res
			}(),
		}, nil
	}
}
