package trip

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*TripUpdate)(nil)

type TripUpdate struct {
	tripDao  *dao.Trip
	idMapper *dao.IdMapper
}

func NewTripUpdateHandler(tripDao *dao.Trip, idMapper *dao.IdMapper) *TripUpdate {
	return &TripUpdate{tripDao: tripDao, idMapper: idMapper}
}

func (c *TripUpdate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.TripUpdate,
		HttpMethod:     http.MethodPut,
		Middlewares:    nil,
		RequestBodyDto: &openapi.TripCreateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Trip{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *TripUpdate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.TripCreateRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}

		carId, err := c.idMapper.GetIdByExtId(ctx, &entities.CarEntity{}, uuid.MustParse(reqRoute.CarId))
		if err != nil {
			return nil, err
		}
		trip, err := c.tripDao.Update(ctx, &entities.Trip{
			ExternalId: uuid.MustParse(*reqRoute.TripId),
			CarId:      carId,
			StartTime:  reqRoute.StartTime,
			EndTime:    reqRoute.EndTime,
			FullPrice:  reqRoute.FullPrice,
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TripEntityToHttp(trip),
		}, nil
	}
}
