package trip

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*RouteList)(nil)

type RouteList struct {
	tripDao *dao.Trip
}

func NewTripListHandler(tripDao *dao.Trip) *RouteList {
	return &RouteList{tripDao: tripDao}
}

func (c *RouteList) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.TripList,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Trip{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteList) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		trips, err := c.tripDao.GetAll(ctx, int(actor.CompanyId))
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Trip {
				res := make([]*openapi.Trip, 0, len(trips))
				for _, car := range trips {
					res = append(res, mappers.TripEntityToHttp(car))
				}
				return res
			}(),
		}, nil
	}
}
