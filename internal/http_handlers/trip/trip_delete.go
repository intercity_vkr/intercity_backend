package trip

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*TripDelete)(nil)

type TripDelete struct {
	tripDao *dao.Car
}

func NewTripDeleteHandler(tripDao *dao.Car) *TripDelete {
	return &TripDelete{tripDao: tripDao}
}

func (c *TripDelete) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.TripDelete,
		HttpMethod:     http.MethodDelete,
		Middlewares:    nil,
		RequestBodyDto: &openapi.CarDeleteRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           entities.ManagerUserType,
	}
}

func (c *TripDelete) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.CarDeleteRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		err := c.tripDao.Delete(ctx, uuid.MustParse(reqRoute.CarId))
		if err != nil {
			return nil, err
		}

		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   "success",
		}, nil
	}
}
