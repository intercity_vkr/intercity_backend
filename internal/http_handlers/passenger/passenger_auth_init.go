package passenger

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*RegisterPassenger)(nil)

type RegisterPassenger struct {
	passengerDao *dao.Passenger
}

func NewPassengerRegisterService(passengerDao *dao.Passenger) *RegisterPassenger {
	return &RegisterPassenger{passengerDao: passengerDao}
}

func (c *RegisterPassenger) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerAuthInit,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.PassengerAuthInitRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           "",
	}
}

func (c *RegisterPassenger) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqClient, ok := requestData.BodyDto.(*openapi.PassengerAuthInitRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		var passenger *entities.PassengerEntity
		var err error
		// регистрация
		if reqClient.FirstName != nil {
			passenger, err = c.passengerDao.CreatePassenger(ctx, &entities.PassengerEntity{
				Phone:     reqClient.Phone,
				FirstName: *reqClient.FirstName,
				LastName:  reqClient.LastName,
			})
			if err != nil {
				// handle error
				return nil, internal_errors.ErrAlreadyExist
			}
		} else { // авторизация
			passenger, err = c.passengerDao.GetByPhone(ctx, reqClient.Phone)
			if err != nil {
				return nil, utils.WrapError(internal_errors.ErrNotFound, "passenger not found")
			}
		}
		verifiedCode := "5555"
		err = c.passengerDao.SetVerifiedCodeByPhone(ctx, passenger.Id, &verifiedCode)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.PassengerIdToHttp(passenger),
		}, nil
	}
}
