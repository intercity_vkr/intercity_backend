package passenger

import (
	"context"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*PassengerSelf)(nil)

type PassengerSelf struct {
}

func NewPassengerSelfHandler() *PassengerSelf {
	return &PassengerSelf{}
}

func (c *PassengerSelf) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerSelf,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           entities.PassengerUserType,
	}
}

func (c *PassengerSelf) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.PassengerEntity](hc.Actor)
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.PassengerSelfToHttp(actor),
		}, nil
	}
}
