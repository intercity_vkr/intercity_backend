package passenger

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*PassengerAuthCode)(nil)

type PassengerAuthCode struct {
	passengerDao *dao.Passenger
	jwtService   *services.Jwt
}

func NewPassengerAuthCodeHandler(passengerDao *dao.Passenger, jwtService *services.Jwt) *PassengerAuthCode {
	return &PassengerAuthCode{passengerDao: passengerDao, jwtService: jwtService}
}

func (c *PassengerAuthCode) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerAuthCode,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.PassengerAuthCodeRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           "",
	}
}

func (c *PassengerAuthCode) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		req, ok := requestData.BodyDto.(*openapi.PassengerAuthCodeRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		externalId, err := uuid.Parse(req.PassengerId)
		if err != nil {
			return nil, internal_errors.ErrInvalidBody
		}

		client, err := c.passengerDao.GetByExternalId(ctx, externalId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrNotFound, "passenger not found")
		}

		if client.VerifiedCode != nil && *client.VerifiedCode != req.Code {
			return nil, utils.WrapError(internal_errors.ErrUnauthorized, "invalid verified code")
		}

		tokens, err := c.jwtService.NewJwtTokenPairs(entities.PassengerUserType, client.ExternalId)
		if err != nil {
			return nil, err
		}

		err = c.passengerDao.SetVerifiedCodeByPhone(ctx, client.Id, nil)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TokenPairEntityToHttp(*tokens),
		}, nil
	}
}
