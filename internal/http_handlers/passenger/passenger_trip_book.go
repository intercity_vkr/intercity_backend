package passenger

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*PassengerTripBook)(nil)

type PassengerTripBook struct {
	tripService *services.Trip
	idMapper    *dao.IdMapper
}

func NewPassengerTripBookHandler(tripService *services.Trip, idMapper *dao.IdMapper) *PassengerTripBook {
	return &PassengerTripBook{tripService: tripService, idMapper: idMapper}
}

func (c *PassengerTripBook) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerTripBook,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.PassengerTripBookRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Trip{},
		Auth:           entities.PassengerUserType,
	}
}

func (c *PassengerTripBook) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		req, ok := requestData.BodyDto.(*openapi.PassengerTripBookRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		tripExternalId, err := uuid.Parse(req.TripId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid trip_id")
		}

		tripId, err := c.idMapper.GetIdByExtId(ctx, &entities.Trip{}, tripExternalId)
		if err != nil {
			return nil, err
		}

		actor := httpserver.ConcreteActor[*entities.PassengerEntity](hc.Actor)

		trip, err := c.tripService.BookSeatInTrip(ctx, tripId, int(actor.Id), req.SeatNumber)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TripEntityToHttp(trip),
		}, nil
	}
}
