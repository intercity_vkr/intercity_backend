package passenger

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*PassengerAuthRefreshToken)(nil)

type PassengerAuthRefreshToken struct {
	jwtService   *services.Jwt
	passengerDao *dao.Passenger
}

func NewPassengerAuthRefreshTokenHandler(jwtService *services.Jwt, passengerDao *dao.Passenger) *PassengerAuthRefreshToken {
	return &PassengerAuthRefreshToken{
		jwtService:   jwtService,
		passengerDao: passengerDao,
	}
}

func (c *PassengerAuthRefreshToken) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PassengerAuthRefreshToken,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.RefreshToken{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           "",
	}
}

func (c *PassengerAuthRefreshToken) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		requestBody := requestData.BodyDto.(*openapi.RefreshToken)
		claims, err := c.jwtService.ParseToken(requestBody.RefreshToken)
		if err != nil {
			return nil, err
		}
		if claims.GetTokenType() != entities.RefreshTokenType || claims.GetUserType() != entities.PassengerUserType {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "bad token")
		}
		extId, err := claims.GetUserExternalId()
		if err != nil {
			return nil, err
		}
		client, err := c.passengerDao.GetByExternalId(ctx, *extId)
		if err != nil || client.Ban {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "bad token")
		}
		tp, err := c.jwtService.NewJwtTokenPairs(entities.PassengerUserType, client.ExternalId)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TokenPairEntityToHttp(*tp),
		}, nil
	}
}
