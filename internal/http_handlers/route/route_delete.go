package route

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*RouteDelete)(nil)

type RouteDelete struct {
	routeDao *dao.Route
	idMapper *dao.IdMapper
}

func NewPlaceDeleteHandler(routeDao *dao.Route, idMapper *dao.IdMapper) *RouteDelete {
	return &RouteDelete{routeDao: routeDao, idMapper: idMapper}
}

func (c *RouteDelete) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.RouteDelete,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.RouteDeleteRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteDelete) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.RouteDeleteRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		routeExternalId, err := uuid.Parse(reqRoute.RouteId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid route id")
		}
		routeId, err := c.idMapper.GetIdByExtId(ctx, &entities.Route{}, routeExternalId)
		if err != nil {
			return nil, err
		}
		err = c.routeDao.DeleteRoute(ctx, routeId)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   "success",
		}, nil
	}
}
