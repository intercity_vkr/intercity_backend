package route

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*RouteUpdate)(nil)

type RouteUpdate struct {
	routeDao *dao.Route
	idMapper *dao.IdMapper
}

func NewRouteUpdateHandler(routeDao *dao.Route, idMapper *dao.IdMapper) *RouteUpdate {
	return &RouteUpdate{routeDao: routeDao, idMapper: idMapper}
}

func (c *RouteUpdate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.RouteUpdate,
		HttpMethod:     http.MethodPut,
		Middlewares:    nil,
		RequestBodyDto: &openapi.RouteUpdateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Route{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteUpdate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.RouteUpdateRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		routeExternalId, err := uuid.Parse(reqRoute.RouteId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid route_id")
		}
		routeId, err := c.idMapper.GetIdByExtId(ctx, &entities.Route{}, routeExternalId)
		if err != nil {
			return nil, err
		}
		route, err := c.routeDao.UpdateRoute(ctx, &entities.Route{
			Id:          routeId,
			Name:        reqRoute.Name,
			Description: reqRoute.Description,
			Stops: func() []entities.Place {
				stops := make([]entities.Place, 0, len(reqRoute.StopsIds))
				for _, stopId := range reqRoute.StopsIds {
					stops = append(stops, entities.Place{ExternalId: uuid.MustParse(stopId)})
				}
				return stops
			}(),
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.RouteEntityToHttp(route),
		}, nil
	}
}
