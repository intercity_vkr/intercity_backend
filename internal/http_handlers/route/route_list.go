package route

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*RouteList)(nil)

type RouteList struct {
	routeDao *dao.Route
}

func NewRouteListHandler(routeDao *dao.Route) *RouteList {
	return &RouteList{routeDao: routeDao}
}

func (c *RouteList) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.RouteList,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Route{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteList) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		cars, err := c.routeDao.GetByFilter(ctx, &filters.RouteFilter{CompanyIds: []int{int(actor.CompanyId)}})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Route {
				res := make([]*openapi.Route, 0, len(cars))
				for _, car := range cars {
					res = append(res, mappers.RouteEntityToHttp(car))
				}
				return res
			}(),
		}, nil
	}
}
