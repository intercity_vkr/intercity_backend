package route

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*RouteCreate)(nil)

type RouteCreate struct {
	routeDao *dao.Route
}

func NewPlaceCreateHandler(routeDao *dao.Route) *RouteCreate {
	return &RouteCreate{routeDao: routeDao}
}

func (c *RouteCreate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.RouteCreate,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.RouteCreateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Route{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteCreate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqRoute, ok := requestData.BodyDto.(*openapi.RouteCreateRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		startPlaceExtId, err := uuid.Parse(reqRoute.StartPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid start_place_id")
		}
		endPlaceExtId, err := uuid.Parse(reqRoute.EndPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid end_place_id")
		}
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		route, err := c.routeDao.CreateRoute(ctx, &entities.Route{
			Name:        reqRoute.Name,
			Description: reqRoute.Description,
			StartPlace:  entities.Place{ExternalId: startPlaceExtId},
			EndPlace:    entities.Place{ExternalId: endPlaceExtId},
			CompanyId:   int(actor.CompanyId),
			Stops: func() []entities.Place {
				stops := make([]entities.Place, 0, len(reqRoute.StopsIds))
				for _, stopId := range reqRoute.StopsIds {
					stops = append(stops, entities.Place{ExternalId: uuid.MustParse(stopId)})
				}
				return stops
			}(),
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.RouteEntityToHttp(route),
		}, nil
	}
}
