package route

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*RouteFilter)(nil)

type RouteFilter struct {
	routeDao *dao.Route
	idMapper *dao.IdMapper
}

func NewRouteFilterHandler(routeDao *dao.Route, idMapper *dao.IdMapper) *RouteFilter {
	return &RouteFilter{routeDao: routeDao, idMapper: idMapper}
}

func (c *RouteFilter) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.RouteFilter,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.RouteFilterRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Route{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *RouteFilter) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		reqFilter, ok := requestData.BodyDto.(*openapi.RouteFilterRequest)
		if !ok {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid filter")
		}
		startPlaceExtId, err := uuid.Parse(reqFilter.StartPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid startPlaceExtId")
		}
		endPlaceExtId, err := uuid.Parse(reqFilter.EndPlaceId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid endPlaceExtId")
		}

		startPlaceId, err := c.idMapper.GetIdByExtId(ctx, &entities.Place{}, startPlaceExtId)
		if err != nil {
			return nil, err
		}
		endPlaceId, err := c.idMapper.GetIdByExtId(ctx, &entities.Place{}, endPlaceExtId)
		if err != nil {
			return nil, err
		}

		stopsIds := make([]int, 0, len(reqFilter.StopsIds))
		for _, stop := range reqFilter.StopsIds {
			stopExtId, err := uuid.Parse(stop)
			if err != nil {
				return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid stopExtId")
			}
			stopId, err := c.idMapper.GetIdByExtId(ctx, &entities.Place{}, stopExtId)
			if err != nil {
				return nil, err
			}
			stopsIds = append(stopsIds, stopId)
		}

		cars, err := c.routeDao.GetByFilter(ctx,
			&filters.RouteFilter{CompanyIds: []int{int(actor.CompanyId)},
				StartPlaceId: &startPlaceId,
				EndPlaceId:   &endPlaceId,
				StopsId:      stopsIds,
			})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Route {
				res := make([]*openapi.Route, 0, len(cars))
				for _, car := range cars {
					res = append(res, mappers.RouteEntityToHttp(car))
				}
				return res
			}(),
		}, nil
	}
}
