package place

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*PlaceList)(nil)

type PlaceList struct {
	placeDao *dao.Place
}

func NewPlaceListHandler(placeDao *dao.Place) *PlaceList {
	return &PlaceList{placeDao: placeDao}
}

func (c *PlaceList) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PlaceList,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []*openapi.Place{},
		Auth:           "",
	}
}

func (c *PlaceList) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		places, err := c.placeDao.GetByFilter(ctx, &filters.PlaceFilter{})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Place {
				res := make([]*openapi.Place, 0, len(places))
				for _, place := range places {
					res = append(res, mappers.PlaceEntityToHttp(place))
				}
				return res
			}(),
		}, nil
	}
}
