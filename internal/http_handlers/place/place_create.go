package place

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*PlaceCreate)(nil)

type PlaceCreate struct {
	placeDao *dao.Place
}

func NewPlaceCreateHandler(placeDao *dao.Place) *PlaceCreate {
	return &PlaceCreate{placeDao: placeDao}
}

func (c *PlaceCreate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.PlaceCreate,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.PlaceCreateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Place{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *PlaceCreate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqPlace, ok := requestData.BodyDto.(*openapi.PlaceCreateRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		place, err := c.placeDao.CreatePlace(ctx, &entities.Place{
			Name:      reqPlace.Name,
			Latitude:  float64(reqPlace.Latitude),
			Longitude: float64(reqPlace.Longitude),
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.PlaceEntityToHttp(place),
		}, nil
	}
}
