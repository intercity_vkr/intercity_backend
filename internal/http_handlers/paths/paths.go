package paths

// authorization

const PassengerAuthRefreshToken = "/passenger/auth/refresh_token"
const PassengerAuthInit = "/passenger/auth/init"
const PassengerAuthCode = "/passenger/auth/code"
const PassengerSelf = "/passenger/me"
const PassengerTripFilter = "/passenger/trip/filter"
const PassengerTripBook = "/passenger/trip/book"

const ManagerAuthInit = "/manager/auth/init"
const ManagerAuthCode = "/manager/auth/code"
const ManagerSelf = "/manager/me"

const CarCreate = "/car/create"
const CarList = "/car/list"
const CarUpdate = "/car/update"
const CarDelete = "/car/delete"

const PlaceCreate = "/place/create"
const PlaceList = "/place/list"

const RouteCreate = "/route/create"
const RouteList = "/route/list"
const RouteFilter = "/route/filter"
const RouteUpdate = "/route/update"
const RouteDelete = "/route/delete"

const TripCreate = "/trip/create"
const TripList = "/trip/list"
const TripUpdate = "/trip/update"
const TripDelete = "/car/delete"
