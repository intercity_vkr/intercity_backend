package car

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*CarList)(nil)

type CarList struct {
	carDao *dao.Car
}

func NewCarListHandler(carDao *dao.Car) *CarList {
	return &CarList{carDao: carDao}
}

func (c *CarList) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.CarList,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    []openapi.Car{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *CarList) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		cars, err := c.carDao.GetByFilter(ctx, &filters.CarFilter{
			CompaniesIds: []int{int(actor.CompanyId)},
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: func() []*openapi.Car {
				res := make([]*openapi.Car, 0, len(cars))
				for _, car := range cars {
					res = append(res, mappers.CarEntityToHttp(car))
				}
				return res
			}(),
		}, nil
	}
}
