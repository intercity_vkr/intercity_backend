package car

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*CarUpdate)(nil)

type CarUpdate struct {
	carDao *dao.Car
}

func NewCarUpdateHandler(carDao *dao.Car) *CarUpdate {
	return &CarUpdate{carDao: carDao}
}

func (c *CarUpdate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.CarUpdate,
		HttpMethod:     http.MethodPut,
		Middlewares:    nil,
		RequestBodyDto: &openapi.CarUpdateRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Car{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *CarUpdate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqCar, ok := requestData.BodyDto.(*openapi.CarUpdateRequest)
		if !ok {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid request")
		}
		carId, err := uuid.Parse(reqCar.CarId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid request")
		}
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		cars, err := c.carDao.GetByFilter(ctx, &filters.CarFilter{
			CompaniesIds: []int{int(actor.CompanyId)},
			ExternalIds:  []uuid.UUID{carId},
		})
		if err != nil {
			return nil, err
		}
		if len(cars) == 0 {
			return nil, utils.WrapError(internal_errors.ErrNotFound, "car not found")
		}
		updatedCar := &entities.CarEntity{
			Id:           cars[0].Id,
			ExternalId:   cars[0].ExternalId,
			CompanyId:    cars[0].CompanyId,
			Mark:         reqCar.Mark,
			Model:        reqCar.Model,
			Gosnomer:     reqCar.Gosnomer,
			Color:        reqCar.Color,
			SeatingChart: reqCar.SeatingChart,
			Company:      cars[0].Company,
		}
		err = c.carDao.Update(ctx, updatedCar)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.CarEntityToHttp(updatedCar),
		}, nil
	}
}
