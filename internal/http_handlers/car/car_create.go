package car

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*CarCreate)(nil)

type CarCreate struct {
	carDao *dao.Car
}

func NewCarCreateHandler(carDao *dao.Car) *CarCreate {
	return &CarCreate{carDao: carDao}
}
func (c *CarCreate) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.CarCreate,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.CreateCarRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Car{},
		Auth:           entities.ManagerUserType,
	}
}
func (c *CarCreate) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqCar, ok := requestData.BodyDto.(*openapi.CreateCarRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		car, err := c.carDao.CreateCar(ctx, &entities.CarEntity{
			CompanyId:    int(actor.CompanyId),
			Mark:         reqCar.Mark,
			Model:        reqCar.Model,
			Gosnomer:     reqCar.Gosnomer,
			Color:        reqCar.Color,
			SeatingChart: reqCar.SeatingChart,
		})
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.CarEntityToHttp(car),
		}, nil
	}
}
