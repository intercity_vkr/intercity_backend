package car

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*CarDelete)(nil)

type CarDelete struct {
	carDao *dao.Car
}

func NewCarDeleteHandler(carDao *dao.Car) *CarDelete {
	return &CarDelete{carDao: carDao}
}
func (c *CarDelete) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.CarCreate,
		HttpMethod:     http.MethodDelete,
		Middlewares:    nil,
		RequestBodyDto: &openapi.CarDeleteRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           entities.ManagerUserType,
	}
}
func (c *CarDelete) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqCar, ok := requestData.BodyDto.(*openapi.CarDeleteRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		carId, err := uuid.Parse(reqCar.CarId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidBody, "invalid request")
		}
		err = c.carDao.Delete(ctx, carId)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
		}, nil
	}
}
