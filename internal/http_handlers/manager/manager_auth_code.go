package manager

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*ManagerAuthCode)(nil)

type ManagerAuthCode struct {
	managerDao *dao.Manager
	jwtService *services.Jwt
}

func NewManagerAuthCodeHandler(managerDao *dao.Manager, jwtService *services.Jwt) *ManagerAuthCode {
	return &ManagerAuthCode{managerDao: managerDao, jwtService: jwtService}
}

func (c *ManagerAuthCode) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.ManagerAuthCode,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.ManagerAuthCodeRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.TokenPair{},
		Auth:           "",
	}
}

func (c *ManagerAuthCode) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		req, ok := requestData.BodyDto.(*openapi.ManagerAuthCodeRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		managerId, err := uuid.Parse(req.ManagerId)
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrInvalidData, err.Error())
		}
		manager, err := c.managerDao.GetByFilter(ctx,
			&filters.ManagerFilter{ExternalIds: []uuid.UUID{managerId}})
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrUnauthorized, "not authorized")
		}
		if manager.VerifiedCode == nil || *manager.VerifiedCode != req.Code {
			return nil, utils.WrapError(internal_errors.ErrUnauthorized, "wrong code")
		}
		tokens, err := c.jwtService.NewJwtTokenPairs(entities.ManagerUserType, uuid.UUID(manager.ExternalId))
		if err != nil {
			return nil, err
		}

		manager.VerifiedCode = nil
		err = c.managerDao.Update(ctx, manager)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.TokenPairEntityToHttp(*tokens),
		}, nil
	}
}
