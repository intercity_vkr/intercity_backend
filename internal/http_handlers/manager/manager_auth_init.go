package manager

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/core/httpserver"
	"intercity_backend/pkg/utils"
	"net/http"
)

var _ httpserver.IHandler = (*ManagerAuthInit)(nil)

type ManagerAuthInit struct {
	managerDao *dao.Manager
}

func NewManagerAuthInitHandler(managerDao *dao.Manager) *ManagerAuthInit {
	return &ManagerAuthInit{managerDao: managerDao}
}

func (c *ManagerAuthInit) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.ManagerAuthInit,
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.ManagerAuthInitRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.ManagerAuthInitResponse{},
		Auth:           "",
	}
}

func (c *ManagerAuthInit) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		reqManager, ok := requestData.BodyDto.(*openapi.ManagerAuthInitRequest)
		if !ok {
			return nil, internal_errors.ErrInvalidBody
		}
		manager, err := c.managerDao.GetByFilter(ctx, &filters.ManagerFilter{
			Phone: &reqManager.Phone,
		})
		if err != nil {
			return nil, utils.WrapError(internal_errors.ErrNotFound, "manager not found")
		}
		verifiedCode := "5555"
		manager.VerifiedCode = &verifiedCode
		err = c.managerDao.Update(ctx, manager)
		if err != nil {
			return nil, err
		}
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data: &openapi.ManagerAuthInitResponse{
				ManagerId: uuid.UUID(manager.ExternalId).String(),
			},
		}, nil
	}
}
