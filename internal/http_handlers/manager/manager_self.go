package manager

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/http_handlers/paths"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*ManagerSelf)(nil)

type ManagerSelf struct {
}

func NewManagerSelfHandler() *ManagerSelf {
	return &ManagerSelf{}
}

func (c *ManagerSelf) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           paths.ManagerSelf,
		HttpMethod:     http.MethodGet,
		Middlewares:    nil,
		RequestBodyDto: nil,
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    &openapi.Manager{},
		Auth:           entities.ManagerUserType,
	}
}

func (c *ManagerSelf) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {
		actor := httpserver.ConcreteActor[*entities.ManagerEntity](hc.Actor)
		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   mappers.ManagerEntityToHttp(actor, &actor.Company),
		}, nil
	}
}
