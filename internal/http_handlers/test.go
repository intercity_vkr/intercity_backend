package http_handlers

import (
	"context"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/entities"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
)

var _ httpserver.IHandler = (*Test)(nil)

type Test struct {
}

func NewTestHandler() *Test {
	return &Test{}
}

func (c *Test) GetConfig() httpserver.IHandlerConfig {
	return &httpserver.HandlerConfig{
		Path:           "/passenger/test",
		HttpMethod:     http.MethodPost,
		Middlewares:    nil,
		RequestBodyDto: &openapi.ClientAuthCodeRequest{},
		PathParamsDto:  nil,
		QueryParamsDto: nil,
		ResponseDto:    nil,
		Auth:           entities.PassengerUserType,
	}
}

func (c *Test) Handler() httpserver.HandlerFunc {
	return func(ctx context.Context, hc *httpserver.HandlerContext,
		requestData *httpserver.RequestData) (*httpserver.ResponseData, error) {

		return &httpserver.ResponseData{
			Status: http.StatusOK,
			Data:   "Hello",
		}, nil
	}
}
