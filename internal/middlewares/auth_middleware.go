package middlewares

import (
	"github.com/google/uuid"
	"intercity_backend/internal/app/config"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
	"strings"
	"time"
)

type AuthMiddleware struct {
	jwtService   *services.Jwt
	passengerDao *dao.Passenger
	managerDao   *dao.Manager
	config       *config.Config
}

func NewAuthMiddleware(jwtService *services.Jwt,
	passengerDao *dao.Passenger,
	managerDao *dao.Manager,
	config *config.Config) *AuthMiddleware {
	return &AuthMiddleware{
		jwtService:   jwtService,
		passengerDao: passengerDao,
		managerDao:   managerDao,
		config:       config,
	}
}

func (a *AuthMiddleware) Middleware(hc *httpserver.HandlerContext, role string) (httpserver.IActor, bool) {
	rawToken := a.GetToken(hc)
	claims, err := a.jwtService.ParseToken(rawToken)
	if err != nil {
		return nil, false
	}
	if claims.GetTokenType() != entities.AccessTokenType || claims.GetUserType() != role || len(claims.Audience) == 0 {
		return nil, false
	}
	if claims.ExpiresAt == nil || claims.ExpiresAt.Before(time.Now()) {
		return nil, false
	}
	switch role {
	case entities.PassengerUserType:
		return a.PassengerFlow(hc, claims)
	case entities.ManagerUserType:
		return a.ManagerFlow(hc, claims)
	default:
		return nil, false
	}
}

func (a *AuthMiddleware) GetToken(hc *httpserver.HandlerContext) string {
	header := hc.GCtx.GetHeader(a.config.HttpAuthorization.AuthHeader)
	v := strings.Split(header, " ")
	if len(v) != 2 {
		return ""
	}
	if v[0] != a.config.HttpAuthorization.AuthHeaderPrefix {
		return ""
	}
	return v[1]
}

func (a *AuthMiddleware) PassengerFlow(hc *httpserver.HandlerContext, claims *entities.UserClaims) (httpserver.IActor, bool) {
	externalId, err := uuid.Parse(claims.Audience[0])
	if err != nil {
		return nil, false
	}
	client, err := a.passengerDao.GetByExternalId(hc.GCtx.Request.Context(), externalId)
	if err != nil {
		return nil, false
	}
	if client.Ban {
		return nil, false
	}
	return client, true
}

func (a *AuthMiddleware) ManagerFlow(hc *httpserver.HandlerContext, claims *entities.UserClaims) (httpserver.IActor, bool) {
	externalId, err := uuid.Parse(claims.Audience[0])
	if err != nil {
		return nil, false
	}
	manager, err := a.managerDao.GetByFilter(hc.GCtx.Request.Context(), &filters.ManagerFilter{
		ExternalIds: []uuid.UUID{externalId}})

	if err != nil {
		return nil, false
	}
	return manager, true
}
