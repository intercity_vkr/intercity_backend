package entities

import (
	"github.com/google/uuid"
	"time"
)

type CarEntity struct {
	Id           int
	ExternalId   uuid.UUID
	CompanyId    int
	Mark         string
	Model        string
	Gosnomer     string
	Color        string
	SeatingChart string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
	Company      CompanyEntity
}
