package entities

import (
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"intercity_backend/internal/entities/ids"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/utils"
	"time"
)

const RefreshTokenType = "refresh"
const AccessTokenType = "access"

type TokenType string

type UserClaims struct {
	jwt.RegisteredClaims
	TokenType TokenType `json:"token_type"`
}

func (uc UserClaims) GetTokenType() TokenType { return uc.TokenType }
func (uc UserClaims) GetUserType() string     { return uc.Subject }
func (uc UserClaims) GetUserExternalId() (*uuid.UUID, error) {
	if len(uc.Audience) != 1 {
		return nil, utils.WrapError(internal_errors.ErrInvalidData, "invalid token")
	}
	extId, err := uuid.Parse(uc.Audience[0])
	if err != nil {
		return nil, utils.WrapError(internal_errors.ErrInvalidData, "invalid token")
	}
	return &extId, nil
}

func NewPassengerClaims(clientExternalId ids.PassengerExternalId, timeLife time.Duration, tokenType TokenType) UserClaims {
	return UserClaims{
		TokenType: tokenType,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "intercity_backend",
			Subject:   PassengerUserType,
			Audience:  jwt.ClaimStrings{uuid.UUID(clientExternalId).String()},
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(timeLife)),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ID:        func() uuid.UUID { v, _ := uuid.NewRandom(); return v }().String(),
		},
	}
}

func NewManagerClaims(managerExternalId ids.ManagerExternalId, timeLife time.Duration, tokenType TokenType) UserClaims {
	return UserClaims{
		TokenType: tokenType,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "intercity_backend",
			Subject:   ManagerUserType,
			Audience:  jwt.ClaimStrings{uuid.UUID(managerExternalId).String()},
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(timeLife)),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ID:        func() uuid.UUID { v, _ := uuid.NewRandom(); return v }().String(),
		},
	}
}

type TokenPair struct {
	AccessToken  string
	RefreshToken string
}
