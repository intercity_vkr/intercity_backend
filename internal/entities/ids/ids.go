package ids

import "github.com/google/uuid"

type Passenger int
type Passengers []Passenger
type PassengerExternalId uuid.UUID
type PassengerExternalIds []PassengerExternalId
type Manager int
type Managers []Manager
type ManagerExternalId uuid.UUID
type ManagerExternalIds []ManagerExternalId
type Company int
type Companies []Company
type CompanyExternalId uuid.UUID
type CompanyExternalIds []CompanyExternalId
