package entities

import (
	"intercity_backend/internal/entities/ids"
	"time"
)

type CompanyEntity struct {
	Id         ids.Company
	ExternalId ids.CompanyExternalId
	Name       string
	Address    string
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time
}
