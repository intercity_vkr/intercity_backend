package entities

import (
	"github.com/google/uuid"
	"intercity_backend/internal/entities/ids"
	"time"
)

type PassengerEntity struct {
	Id           ids.Passenger
	Phone        string
	FirstName    string
	LastName     *string
	ExternalId   uuid.UUID
	Ban          bool
	VerifiedCode *string
	VerifiedAt   *time.Time
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
}

func (c *PassengerEntity) GetId() int {
	return int(c.Id)
}

func (c *PassengerEntity) GetExternalId() uuid.UUID {
	return c.ExternalId
}

func (c *PassengerEntity) GetActorType() string {
	return PassengerUserType
}
