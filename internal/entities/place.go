package entities

import (
	"github.com/google/uuid"
	"time"
)

type Place struct {
	Id         int
	ExternalId uuid.UUID
	Name       string
	Latitude   float64
	Longitude  float64
	CreatedAt  time.Time
	UpdatedAt  time.Time
	DeletedAt  *time.Time
}
