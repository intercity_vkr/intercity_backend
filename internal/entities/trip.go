package entities

import (
	"github.com/google/uuid"
	"time"
)

const (
	SeatVacant   = 'V'
	SeatOccupied = 'O'
)

type Trip struct {
	Id           int
	ExternalId   uuid.UUID
	CompanyId    int
	RouteId      int
	Reversed     bool
	CarId        int
	StartTime    time.Time
	EndTime      time.Time
	FullPrice    int
	SeatingChart string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time

	Company    CompanyEntity
	Route      Route
	Car        CarEntity
	Passengers []TripPassenger
}

func (t *Trip) CheckSeatVacant(seatNumber int) bool {
	if seatNumber >= len(t.SeatingChart) {
		return false
	}
	if seatNumber < 0 {
		return false
	}
	if t.SeatingChart[seatNumber] != SeatVacant {
		return false
	}
	return true
}

func (t *Trip) OccupySeat(seatNumber int) bool {
	if t.CheckSeatVacant(seatNumber) {
		r := []rune(t.SeatingChart)
		r[seatNumber] = SeatOccupied
		t.SeatingChart = string(r)
	} else {
		return false
	}
	return true
}

type TripPassenger struct {
	Id int

	TripId      int
	PassengerId int
	SeatNumber  *int
	Price       *int
	EndPlaceId  int
	Approved    bool
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time

	Passenger PassengerEntity
	EndPlace  Place
}
