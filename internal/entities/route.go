package entities

import (
	"github.com/google/uuid"
	"time"
)

type Route struct {
	Id          int
	ExternalId  uuid.UUID
	Name        string
	Description *string
	CompanyId   int
	Stops       []Place
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
	StartPlace  Place
	EndPlace    Place
}
