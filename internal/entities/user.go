package entities

type UserType string

const PassengerUserType = "passenger"
const ManagerUserType = "manager"
