package entities

import (
	"github.com/google/uuid"
	"intercity_backend/internal/entities/ids"
	"time"
)

type ManagerEntity struct {
	Id           ids.Manager
	ExternalId   ids.ManagerExternalId
	Phone        string
	FirstName    string
	LastName     string
	CompanyId    ids.Company
	VerifiedCode *string
	VerifiedAt   *time.Time
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time
	Company      CompanyEntity
}

func (c *ManagerEntity) GetId() int {
	return int(c.Id)
}

func (c *ManagerEntity) GetExternalId() uuid.UUID {
	return uuid.UUID(c.ExternalId)
}

func (c *ManagerEntity) GetActorType() string {
	return ManagerUserType
}
