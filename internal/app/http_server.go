package app

import (
	"context"
	"errors"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/core/httpserver"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func (a *App) RunHttpServer() {
	server := httpserver.HttpServer{}
	server.InitRouter()
	server.AddGlobalCorsMiddleware()
	server.InitErrorMapper(internal_errors.InternalToHttpError())
	server.AddCommonMiddlewares(a.serviceProvider.CommonMiddlewares)
	server.AddHandlers(a.serviceProvider.HttpHandlers)
	server.InitServer(httpserver.Config{Port: a.serviceProvider.ConfigMain.HttpServer.Port})

	go func() {
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			a.serviceProvider.Logger.Fatal().Err(err).Msg("failed run server")
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	a.serviceProvider.Logger.Info().Msg("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		a.serviceProvider.Logger.Fatal().Err(err).Msg("Server Shutdown")
	}

	select {
	case <-ctx.Done():
		a.serviceProvider.Logger.Info().Msg("timeout of 5 seconds.")
	}
	a.serviceProvider.Logger.Info().Msg("Server exiting")
}
