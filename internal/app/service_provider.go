package app

import (
	"context"
	"database/sql"
	"entgo.io/ent/dialect"
	entsql "entgo.io/ent/dialect/sql"
	"fmt"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/rs/zerolog"
	"intercity_backend/gen/ent"
	"intercity_backend/internal/app/config"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/http_handlers/car"
	"intercity_backend/internal/http_handlers/manager"
	"intercity_backend/internal/http_handlers/passenger"
	"intercity_backend/internal/http_handlers/place"
	"intercity_backend/internal/http_handlers/route"
	"intercity_backend/internal/http_handlers/trip"
	"intercity_backend/internal/middlewares"
	"intercity_backend/internal/services"
	"intercity_backend/pkg/core/httpserver"
)

type ServiceProvider struct {
	CommonMiddlewares httpserver.CommonMiddlewares
	HttpHandlers      httpserver.IHandlers
	Logger            *zerolog.Logger
	ConfigMain        *config.Config
	DbClient          *ent.Client
	Dao               *Dao
	Services          *Services
}

type Dao struct {
	IdMapper  *dao.IdMapper
	Passenger *dao.Passenger
	Manager   *dao.Manager
	Company   *dao.Company
	Car       *dao.Car
	Place     *dao.Place
	Route     *dao.Route
	Trip      *dao.Trip
}

type Services struct {
	Jwt  *services.Jwt
	Trip *services.Trip
}

func NewServiceProvider(_ context.Context) *ServiceProvider {
	return &ServiceProvider{}
}

func (sp *ServiceProvider) InitConfigMain(_ context.Context) error {
	cfg, err := config.NewConfig("config/config.yml")
	if err != nil {
		return err
	}
	sp.ConfigMain = cfg
	return nil
}

func (sp *ServiceProvider) InitLogger(ctx context.Context) error {
	sp.Logger = InitLogger(ctx)
	return nil
}

func (sp *ServiceProvider) InitDb(ctx context.Context) error {
	//database := db.NewDatabase(ctx, db.Config{
	//	Username: sp.ConfigMain.DB.Username,
	//	Password: sp.ConfigMain.DB.Password,
	//	Host:     sp.ConfigMain.DB.Host,
	//	Port:     sp.ConfigMain.DB.Port,
	//	DbName:   sp.ConfigMain.DB.DbName,
	//})
	//err := database.CreatePool(ctx)
	//if err != nil {
	//	return err
	//}
	//sp.DbClient = database
	db, err := sql.Open("pgx", fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		sp.ConfigMain.DB.Username, sp.ConfigMain.DB.Password, sp.ConfigMain.DB.Host, sp.ConfigMain.DB.Port, sp.ConfigMain.DB.DbName))
	if err != nil {
		return err
	}
	drv := entsql.OpenDB(dialect.Postgres, db)
	sp.DbClient = ent.NewClient(ent.Driver(drv))
	return nil
}

func (sp *ServiceProvider) InitDao(_ context.Context) error {
	sp.Dao = &Dao{
		Passenger: dao.NewPassengerDao(sp.DbClient.Passenger),
		Manager:   dao.NewManagerDao(sp.DbClient.Manager),
		Company:   dao.NewCompanyDao(sp.DbClient.Company),
		Car:       dao.NewCarDao(sp.DbClient.Car),
		Place:     dao.NewPlaceDao(sp.DbClient.Place),
		Route:     dao.NewRouteDao(sp.DbClient.Route, sp.DbClient.Place),
		IdMapper:  dao.NewIdMapper(sp.DbClient),
	}
	sp.Dao.Trip = dao.NewTripDao(sp.DbClient.Trip, sp.DbClient.TripPassenger, sp.Dao.Car)
	return nil
}

func (sp *ServiceProvider) InitServices(_ context.Context) error {
	sp.Services = &Services{
		Jwt:  services.NewJwtService(sp.ConfigMain),
		Trip: services.NewTripService(sp.Dao.Trip),
	}
	return nil
}

func (sp *ServiceProvider) InitCommonMiddlewares(_ context.Context) error {
	sp.CommonMiddlewares = httpserver.CommonMiddlewares{
		AuthMiddleware: middlewares.NewAuthMiddleware(sp.Services.Jwt, sp.Dao.Passenger, sp.Dao.Manager, sp.ConfigMain),
	}
	return nil
}

func (sp *ServiceProvider) InitHandlers(_ context.Context) error {
	sp.HttpHandlers = httpserver.IHandlers{
		passenger.NewPassengerRegisterService(sp.Dao.Passenger),
		passenger.NewPassengerAuthCodeHandler(sp.Dao.Passenger, sp.Services.Jwt),
		passenger.NewPassengerAuthRefreshTokenHandler(sp.Services.Jwt, sp.Dao.Passenger),
		passenger.NewPassengerSelfHandler(),
		passenger.NewPassengerTripBookHandler(sp.Services.Trip, sp.Dao.IdMapper),
		manager.NewManagerAuthInitHandler(sp.Dao.Manager),
		manager.NewManagerAuthCodeHandler(sp.Dao.Manager, sp.Services.Jwt),
		manager.NewManagerSelfHandler(),
		car.NewCarCreateHandler(sp.Dao.Car),
		car.NewCarListHandler(sp.Dao.Car),
		car.NewCarUpdateHandler(sp.Dao.Car),
		car.NewCarDeleteHandler(sp.Dao.Car),
		place.NewPlaceCreateHandler(sp.Dao.Place),
		place.NewPlaceListHandler(sp.Dao.Place),
		route.NewPlaceCreateHandler(sp.Dao.Route),
		route.NewRouteListHandler(sp.Dao.Route),
		route.NewRouteFilterHandler(sp.Dao.Route, sp.Dao.IdMapper),
		route.NewRouteUpdateHandler(sp.Dao.Route, sp.Dao.IdMapper),
		route.NewPlaceDeleteHandler(sp.Dao.Route, sp.Dao.IdMapper),
		trip.NewTripCreateHandler(sp.Dao.Trip, sp.Dao.IdMapper),
		trip.NewTripUpdateHandler(sp.Dao.Trip, sp.Dao.IdMapper),
		trip.NewTripListHandler(sp.Dao.Trip),
		trip.NewTripDeleteHandler(sp.Dao.Car),
		trip.NewPassengerTripFilterHandler(sp.Dao.Trip, sp.Dao.IdMapper),
	}
	return nil
}
