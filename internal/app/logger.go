package app

import (
	"context"
	"github.com/rs/zerolog"
	"os"
)

func InitLogger(ctx context.Context) *zerolog.Logger {
	logger := zerolog.New(os.Stdout).With().Ctx(ctx).Timestamp().Logger()
	return &logger
}
