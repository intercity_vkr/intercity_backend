package config

import (
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	HttpServer struct {
		Port int `yaml:"port"`
	} `yaml:"http_server"`
	DB struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		DbName   string `yaml:"db_name"`
	} `yaml:"db"`
	HttpAuthorization struct {
		SecretKey        string `yaml:"secret_key"`
		AccessTokenTL    string `yaml:"access_token_tl"`
		RefreshTokenTL   string `yaml:"refresh_token_tl"`
		AuthHeader       string `yaml:"auth_header"`
		AuthHeaderPrefix string `yaml:"auth_header_prefix"`
	} `yaml:"http_authorization"`
}

func NewConfig(configPath string) (*Config, error) {
	config := Config{}
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	decoder := yaml.NewDecoder(file)
	if err := decoder.Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}
