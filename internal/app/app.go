package app

import "context"

type App struct {
	serviceProvider *ServiceProvider
}

func NewApp() *App {
	return &App{}
}

func (a *App) InitApp(ctx context.Context) error {
	a.serviceProvider = NewServiceProvider(ctx)
	err := a.initDeps(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) initDeps(ctx context.Context) error {
	deps := []func(ctx context.Context) error{
		a.serviceProvider.InitConfigMain,
		a.serviceProvider.InitLogger,
		a.serviceProvider.InitDb,
		a.serviceProvider.InitDao,
		a.serviceProvider.InitServices,
		a.serviceProvider.InitCommonMiddlewares,
		a.serviceProvider.InitHandlers,
	}

	for _, dep := range deps {
		err := dep(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *App) Run() error {
	a.RunHttpServer()
	return nil
}
