package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Passenger holds the schema definition for the Passenger entity.
type Passenger struct {
	ent.Schema
}

// Fields of the Passenger.
func (Passenger) Fields() []ent.Field {
	return []ent.Field{
		field.String("phone").NotEmpty().Unique(),
		field.String("first_name").NotEmpty(),
		field.String("last_name").Nillable().Optional(),
		field.Bool("ban").Default(false),
		field.String("verified_code").Nillable().Optional(),
		field.Time("verified_at").Nillable().Optional(),
	}
}

// Edges of the Passenger.
func (Passenger) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("trips", Trip.Type).Ref("passengers").Through("trip_passengers", TripPassenger.Type),
	}
}

func (Passenger) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
		ExternalIdMixin{},
	}
}
