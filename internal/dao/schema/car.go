package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Car holds the schema definition for the Car entity.
type Car struct {
	ent.Schema
}

// Fields of the Car.
func (Car) Fields() []ent.Field {
	return []ent.Field{
		field.String("mark"),
		field.String("model"),
		field.String("gosnomer"),
		field.String("color"),
		field.String("seating_chart"),
	}
}

// Edges of the Car.
func (Car) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("company", Company.Type).Ref("cars").Unique().Required().Field("company_id").Immutable(),
		edge.To("trips", Trip.Type),
	}
}

func (Car) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
		ExternalIdMixin{},
		CompanyRelationMixin{},
	}
}
