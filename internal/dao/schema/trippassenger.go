package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// TripPassenger holds the schema definition for the TripPassenger entity.
type TripPassenger struct {
	ent.Schema
}

// Fields of the TripPassenger.
func (TripPassenger) Fields() []ent.Field {
	return []ent.Field{
		field.Int("trip_id").Immutable(),
		field.Int("passenger_id").Immutable(),
		field.Int("seat_number").Nillable().Optional(),
		field.Int("price").Nillable().Optional(),
		field.Int("end_place_id").Immutable(),
		field.Bool("approved").Default(false),
	}
}

// Edges of the TripPassenger.
func (TripPassenger) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("trips", Trip.Type).Field("trip_id").Unique().Required().Immutable(),
		edge.To("passenger", Passenger.Type).Field("passenger_id").Unique().Required().Immutable(),
		edge.To("end_place", Place.Type).Field("end_place_id").Unique().Required().Immutable(),
	}
}

func (TripPassenger) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
	}
}
