package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// RouteStop holds the schema definition for the RouteStop entity.
type RouteStop struct {
	ent.Schema
}

// Fields of the RouteStop.
func (RouteStop) Fields() []ent.Field {
	return []ent.Field{
		field.Int("route_id"),
		field.Int("place_id"),
	}
}

// Edges of the RouteStop.
func (RouteStop) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("route", Route.Type).Unique().Required().Field("route_id"),
		edge.To("place", Place.Type).Unique().Required().Field("place_id"),
	}
}

func (RouteStop) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
	}
}
