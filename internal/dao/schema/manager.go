package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Manager holds the schema definition for the Manager entity.
type Manager struct {
	ent.Schema
}

// Fields of the Manager.
func (Manager) Fields() []ent.Field {
	return []ent.Field{
		field.String("phone").NotEmpty().Unique(),
		field.String("first_name").NotEmpty(),
		field.String("last_name").NotEmpty(),
		field.String("verified_code").Nillable().Optional(),
		field.Time("verified_at").Nillable().Optional(),
	}
}

// Edges of the Manager.
func (Manager) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("company", Company.Type).
			Ref("managers").Unique().Field("company_id").Required().Immutable(),
	}
}

func (Manager) Mixin() []ent.Mixin {
	return []ent.Mixin{
		CompanyRelationMixin{},
		ExternalIdMixin{},
		TimeMixin{},
	}
}
