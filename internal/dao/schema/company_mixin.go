package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
)

type CompanyRelationMixin struct {
	mixin.Schema
}

func (CompanyRelationMixin) Fields() []ent.Field {
	return []ent.Field{
		field.Int("company_id").Immutable(),
	}
}
