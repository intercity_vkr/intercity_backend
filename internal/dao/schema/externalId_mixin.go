package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
	"github.com/google/uuid"
)

type ExternalIdMixin struct {
	mixin.Schema
}

func (ExternalIdMixin) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("external_id", uuid.UUID{}).Default(uuid.New).Immutable().Unique(),
	}
}
