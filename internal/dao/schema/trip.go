package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Trip holds the schema definition for the Trip entity.
type Trip struct {
	ent.Schema
}

// Fields of the Trip.
func (Trip) Fields() []ent.Field {
	return []ent.Field{
		field.Int("route_id").Immutable(),
		field.Bool("reversed"),
		field.Int("car_id"),
		field.Time("start_time"),
		field.Time("end_time"),
		field.Int("full_price"),
		field.Text("seating_chart"),
	}
}

// Edges of the Trip.
func (Trip) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("company", Company.Type).
			Ref("trips").Unique().Field("company_id").Required().Immutable(),
		edge.From("route", Route.Type).Ref("trips").Unique().Field("route_id").Required().Immutable(),
		edge.From("car", Car.Type).Ref("trips").Unique().Field("car_id").Required(),
		edge.To("passengers", Passenger.Type).Through("trip_passengers", TripPassenger.Type),
	}
}

func (Trip) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
		ExternalIdMixin{},
		CompanyRelationMixin{},
	}
}
