package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Route holds the schema definition for the Route entity.
type Route struct {
	ent.Schema
}

// Fields of the Route.
func (Route) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.String("description").Nillable().Optional(),
		field.Int("start_place_id").Immutable(),
		field.Int("end_place_id").Immutable(),
	}
}

// Edges of the Route.
func (Route) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("company", Company.Type).
			Ref("routes").Unique().Field("company_id").Required().Immutable(),
		edge.From("start_place", Place.Type).Ref("routes_start").Field("start_place_id").Unique().Required().Immutable(),
		edge.From("end_place", Place.Type).Ref("routes_end").Field("end_place_id").Unique().Required().Immutable(),
		edge.To("stops", Place.Type).Through("route_stops", RouteStop.Type),
		edge.To("trips", Trip.Type),
	}
}

func (Route) Mixin() []ent.Mixin {
	return []ent.Mixin{
		TimeMixin{},
		ExternalIdMixin{},
		CompanyRelationMixin{},
	}
}
