package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Place holds the schema definition for the Place entity.
type Place struct {
	ent.Schema
}

// Fields of the Place.
func (Place) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").NotEmpty(),
		field.Float("longitude"),
		field.Float("latitude"),
	}
}

// Edges of the Place.
func (Place) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("routes_start", Route.Type),
		edge.To("routes_end", Route.Type),
		edge.From("stops", Route.Type).Ref("stops").Through("route_stops", RouteStop.Type),
	}
}

func (Place) Mixin() []ent.Mixin {
	return []ent.Mixin{
		ExternalIdMixin{},
		TimeMixin{},
	}
}
