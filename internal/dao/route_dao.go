package dao

import (
	"context"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/ent/place"
	routeEnt "intercity_backend/gen/ent/route"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/mappers"
	"time"
)

type Route struct {
	db      *ent.RouteClient
	placeDb *ent.PlaceClient
}

func NewRouteDao(db *ent.RouteClient, placeDb *ent.PlaceClient) *Route {
	return &Route{db: db, placeDb: placeDb}
}

func (d *Route) CreateRoute(ctx context.Context, route *entities.Route) (*entities.Route, error) {
	routeDto, err := d.db.Create().
		SetName(route.Name).
		SetNillableDescription(route.Description).
		SetCompanyID(route.CompanyId).
		SetStartPlaceID(d.placeDb.Query().Where(place.ExternalID(route.StartPlace.ExternalId)).FirstIDX(ctx)).
		SetEndPlaceID(d.placeDb.Query().Where(place.ExternalID(route.EndPlace.ExternalId)).FirstIDX(ctx)).
		AddStopIDs(func() []int {
			var ids []int
			for _, stop := range route.Stops {
				ids = append(ids, d.placeDb.Query().Where(place.ExternalID(stop.ExternalId)).FirstIDX(ctx))
			}
			return ids
		}()...).
		Save(ctx)
	routeDto, err = d.db.Query().WithCompany().WithStartPlace().WithEndPlace().WithStops().Where(routeEnt.ID(routeDto.ID)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.RouteDtoToEntity(routeDto), nil
}

func (d *Route) GetByFilter(ctx context.Context, filter *filters.RouteFilter) ([]*entities.Route, error) {
	routeDtos, err := d.db.Query().
		WithCompany().
		WithStartPlace().
		WithEndPlace().
		WithStops().
		Where(filter.GetPredicates()...).
		Where(routeEnt.DeletedAtIsNil()).
		All(ctx)
	if err != nil {
		return nil, err
	}
	routes := make([]*entities.Route, 0, len(routeDtos))
	for _, routeDto := range routeDtos {
		routes = append(routes, mappers.RouteDtoToEntity(routeDto))
	}
	return routes, nil
}

func (d *Route) UpdateRoute(ctx context.Context, route *entities.Route) (*entities.Route, error) {
	routeDto, err := d.db.UpdateOneID(route.Id).
		SetName(route.Name).
		SetNillableDescription(route.Description).
		SetUpdatedAt(time.Now()).
		ClearRouteStops().
		AddStopIDs(func() []int {
			var ids []int
			for _, stop := range route.Stops {
				ids = append(ids, d.placeDb.Query().Where(place.ExternalID(stop.ExternalId)).FirstIDX(ctx))
			}
			return ids
		}()...).
		Save(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.RouteDtoToEntity(routeDto), nil
}

func (d *Route) DeleteRoute(ctx context.Context, routeId int) error {
	return d.db.UpdateOneID(routeId).SetDeletedAt(time.Now()).Exec(ctx)
}
