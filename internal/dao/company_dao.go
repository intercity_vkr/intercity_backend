package dao

import (
	"context"
	"intercity_backend/gen/ent"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/mappers"
)

type Company struct {
	db *ent.CompanyClient
}

func NewCompanyDao(db *ent.CompanyClient) *Company {
	return &Company{db: db}
}

func (d *Company) CreateCompany(ctx context.Context, company *entities.CompanyEntity) (*entities.CompanyEntity, error) {
	companyDto, err := d.db.Create().SetName(company.Name).SetAddress(company.Address).Save(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.CompanyDtoToEntity(companyDto), nil
}

func (d *Company) GetByFilter(ctx context.Context, filter *filters.CompanyFilter) (*entities.CompanyEntity, error) {
	companyDto, err := d.db.Query().Where(filter.GetPredicates()...).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.CompanyDtoToEntity(companyDto), nil
}
