package dao

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/ent/passenger"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/entities/ids"
	"intercity_backend/internal/mappers"
	"time"
)

type Passenger struct {
	db *ent.PassengerClient
}

func (c *Passenger) CreatePassenger(ctx context.Context, passenger *entities.PassengerEntity) (*entities.PassengerEntity, error) {
	passengerDto := c.db.Create().SetPhone(passenger.Phone).
		SetFirstName(passenger.FirstName).
		SetNillableLastName(passenger.LastName).
		SetBan(passenger.Ban).
		SetNillableVerifiedCode(passenger.VerifiedCode).
		SetNillableVerifiedAt(passenger.VerifiedAt).
		SaveX(ctx)

	return mappers.PassengerDtoToEntity(passengerDto), nil
}

func (c *Passenger) GetByPhone(ctx context.Context, phone string) (*entities.PassengerEntity, error) {
	passengerDto, err := c.db.Query().Where(passenger.Phone(phone)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.PassengerDtoToEntity(passengerDto), nil
}

func (c *Passenger) GetByExternalId(ctx context.Context, externalId uuid.UUID) (*entities.PassengerEntity, error) {
	passengerDto, err := c.db.Query().Where(passenger.ExternalID(externalId)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.PassengerDtoToEntity(passengerDto), nil
}

func (c *Passenger) SetVerifiedCodeByPhone(ctx context.Context, id ids.Passenger, code *string) error {
	query := c.db.UpdateOneID(int(id))
	if code != nil {
		query = query.SetVerifiedCode(*code).
			SetVerifiedAt(time.Now())
	} else {
		query = query.ClearVerifiedCode()
	}
	_, err := query.Save(ctx)
	if err != nil {
		return err
	}
	return nil
}

func NewPassengerDao(db *ent.PassengerClient) *Passenger {
	return &Passenger{
		db: db,
	}
}
