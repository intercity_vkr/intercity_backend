package dao

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/ent/company"
	"intercity_backend/gen/ent/route"
	"intercity_backend/gen/ent/trip"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/internal/mappers"
	"intercity_backend/pkg/utils"
	"time"
)

type Trip struct {
	db              *ent.TripClient
	tripPassengerDb *ent.TripPassengerClient
	carDao          *Car
}

func NewTripDao(db *ent.TripClient, tripPassengerDb *ent.TripPassengerClient, carDao *Car) *Trip {
	return &Trip{db: db, carDao: carDao, tripPassengerDb: tripPassengerDb}
}

func (t *Trip) CreateTrip(ctx context.Context, entity *entities.Trip) (*entities.Trip, error) {
	car, err := t.carDao.GetByFilter(ctx, &filters.CarFilter{Ids: []int{entity.CarId}})
	if err != nil {
		return nil, err
	}
	if len(car) == 0 {
		return nil, utils.WrapError(internal_errors.ErrNotFound, "car not found")
	}
	tripDto, err := t.db.Create().
		SetCompanyID(entity.CompanyId).
		SetRouteID(entity.RouteId).
		SetReversed(entity.Reversed).
		SetCarID(entity.CarId).
		SetStartTime(entity.StartTime).
		SetEndTime(entity.EndTime).
		SetFullPrice(entity.FullPrice).
		SetSeatingChart(car[0].SeatingChart).
		Save(ctx)
	if err != nil {
		return nil, err
	}
	a, err := t.db.Query().WithCompany().
		WithCar(func(query *ent.CarQuery) {
			query.WithCompany()
		}).
		WithRoute(func(query *ent.RouteQuery) {
			query.WithStartPlace().WithEndPlace().WithStops()
		}).
		WithTripPassengers().
		WithPassengers().Where(trip.ID(tripDto.ID)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.TripDtoToEntity(a), nil
}

func (t *Trip) GetAll(ctx context.Context, companyId int) ([]*entities.Trip, error) {
	tripDtos, err := t.db.Query().
		WithCompany().
		WithCar(func(query *ent.CarQuery) {
			query.WithCompany()
		}).
		WithRoute(func(query *ent.RouteQuery) {
			query.WithStartPlace().WithEndPlace().WithStops()
		}).
		WithTripPassengers(func(query *ent.TripPassengerQuery) {
			query.WithPassenger().WithEndPlace()
		}).Where(trip.HasCompanyWith(company.ID(companyId))).All(ctx)
	if err != nil {
		return nil, err
	}
	var trips []*entities.Trip

	for _, v := range tripDtos {
		trips = append(trips, mappers.TripDtoToEntity(v))
	}

	return trips, nil
}

func (t *Trip) GetById(ctx context.Context, id int) (*entities.Trip, error) {
	tripDto, err := t.db.Query().
		WithCompany().
		WithCar(func(query *ent.CarQuery) {
			query.WithCompany()
		}).
		WithRoute(func(query *ent.RouteQuery) {
			query.WithStartPlace().WithEndPlace().WithStops()
		}).
		WithTripPassengers(func(query *ent.TripPassengerQuery) {
			query.WithPassenger().WithEndPlace()
		}).Where(trip.ID(id)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.TripDtoToEntity(tripDto), nil
}

func (t *Trip) SearchForPassenger(ctx context.Context,
	startPlaceId, endPlaceId int,
	startTime, endTime time.Time) ([]*entities.Trip, error) {
	tripDtos, err := t.db.Query().
		WithCompany().
		WithCar(func(query *ent.CarQuery) {
			query.WithCompany()
		}).
		WithRoute(func(query *ent.RouteQuery) {
			query.WithStartPlace().WithEndPlace().WithStops()
		}).
		WithTripPassengers(func(query *ent.TripPassengerQuery) {
			query.WithPassenger().WithEndPlace()
		}).
		Where(
			trip.StartTimeGTE(startTime), trip.EndTimeLTE(endTime),
			trip.Or(
				trip.HasRouteWith(
					route.And(route.StartPlaceID(startPlaceId), route.EndPlaceID(endPlaceId))),
				trip.And(
					trip.HasRouteWith(
						route.And(route.StartPlaceID(endPlaceId), route.EndPlaceID(startPlaceId)),
					),
					trip.Reversed(true),
				),
			),
		).
		All(ctx)
	if err != nil {
		return nil, err
	}
	var trips []*entities.Trip

	for _, v := range tripDtos {
		trips = append(trips, mappers.TripDtoToEntity(v))
	}
	return trips, nil
}

func (t *Trip) AddPassenger(ctx context.Context, tripId, passengerId, seatNumber, price, endPlaceId int) error {
	_, err := t.tripPassengerDb.Create().
		SetTripID(tripId).
		SetPassengerID(passengerId).
		SetSeatNumber(seatNumber).
		SetPrice(price).
		SetEndPlaceID(endPlaceId).Save(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (t *Trip) Update(ctx context.Context, entity *entities.Trip) (*entities.Trip, error) {
	_, err := t.db.Update().
		SetCarID(entity.CarId).
		SetStartTime(entity.StartTime).
		SetEndTime(entity.EndTime).
		SetFullPrice(entity.FullPrice).
		SetSeatingChart(entity.SeatingChart).
		Where(trip.ExternalID(entity.ExternalId)).
		Save(ctx)
	if err != nil {
		return nil, err
	}
	tripDto, err := t.db.Query().WithCar(func(query *ent.CarQuery) {
		query.WithCompany()
	}).
		WithRoute(func(query *ent.RouteQuery) {
			query.WithStartPlace().WithEndPlace().WithStops()
		}).
		WithCompany().
		WithTripPassengers(func(query *ent.TripPassengerQuery) {
			query.WithPassenger().WithEndPlace()
		}).Where(trip.ExternalID(entity.ExternalId)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.TripDtoToEntity(tripDto), nil
}

func (t *Trip) Delete(ctx context.Context, extId uuid.UUID) error {
	_, err := t.db.Delete().
		Where(trip.ExternalID(extId)).
		Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}
