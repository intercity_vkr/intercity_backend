package dao

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/ent/car"
	"intercity_backend/gen/ent/passenger"
	"intercity_backend/gen/ent/place"
	"intercity_backend/gen/ent/route"
	"intercity_backend/gen/ent/trip"
	"intercity_backend/internal/entities"
)

type IdMapper struct {
	db *ent.Client
}

func NewIdMapper(db *ent.Client) *IdMapper {
	return &IdMapper{db: db}
}

func (i *IdMapper) GetIdByExtId(ctx context.Context, entity any, extId uuid.UUID) (int, error) {
	switch entity.(type) {
	case *entities.PassengerEntity:
		return i.db.Passenger.Query().Where(passenger.ExternalID(extId)).OnlyID(ctx)
	case *entities.Place:
		return i.db.Place.Query().Where(place.ExternalID(extId)).OnlyID(ctx)
	case *entities.Route:
		return i.db.Route.Query().Where(route.ExternalID(extId)).OnlyID(ctx)
	case *entities.CarEntity:
		return i.db.Car.Query().Where(car.ExternalID(extId)).OnlyID(ctx)
	case *entities.Trip:
		return i.db.Trip.Query().Where(trip.ExternalID(extId)).OnlyID(ctx)
	default:
		panic(fmt.Sprintf("[IdMapper.GetIdByExtId] unknown entity type: %T", entity))
	}
}
