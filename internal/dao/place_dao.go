package dao

import (
	"context"
	"intercity_backend/gen/ent"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/mappers"
)

type Place struct {
	db *ent.PlaceClient
}

func NewPlaceDao(db *ent.PlaceClient) *Place {
	return &Place{db: db}
}

func (d *Place) CreatePlace(ctx context.Context, place *entities.Place) (*entities.Place, error) {
	placeDto, err := d.db.Create().
		SetName(place.Name).
		SetLatitude(place.Latitude).
		SetLongitude(place.Longitude).
		Save(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.PlaceDtoToEntity(placeDto), nil
}

func (d *Place) GetByFilter(ctx context.Context, filter *filters.PlaceFilter) ([]*entities.Place, error) {
	placeDtos, err := d.db.Query().Where(filter.GetPredicates()...).All(ctx)
	if err != nil {
		return nil, err
	}

	places := make([]*entities.Place, 0, len(placeDtos))
	for _, placeDto := range placeDtos {
		places = append(places, mappers.PlaceDtoToEntity(placeDto))
	}
	return places, nil
}
