package dto

import (
	"github.com/google/uuid"
	"time"
)

const PassengerTableName = "passenger"

const (
	PassengerDtoIdColumnName           = "id"
	PassengerDtoExternalIdColumnName   = "external_id"
	PassengerDtoPhoneColumnName        = "phone"
	PassengerDtoFirstNameColumnName    = "first_name"
	PassengerDtoLastNameColumnName     = "last_name"
	PassengerDtoBanColumnName          = "ban"
	PassengerDtoVerifiedCodeColumnName = "verified_code"
	PassengerDtoVerifiedAtColumnName   = "verified_at"
	PassengerDtoCreatedAtColumnName    = "created_at"
	PassengerDtoUpdatedAtColumnName    = "updated_at"
	PassengerDtoDeletedAtColumnName    = "deleted_at"
)

type PassengerDto struct {
	Id           int        `db:"id"`
	ExternalId   uuid.UUID  `db:"external_id"`
	Phone        string     `db:"phone"`
	FirstName    string     `db:"first_name"`
	LastName     *string    `db:"last_name"`
	Ban          bool       `db:"ban"`
	VerifiedCode *string    `db:"verified_code"`
	VerifiedAt   *time.Time `db:"verified_at"`
	CreatedAt    time.Time  `db:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at"`
	DeletedAt    *time.Time `db:"deleted_at"`
}

func (d PassengerDto) ToSetMap() map[string]any {
	return map[string]any{
		PassengerDtoPhoneColumnName:     d.Phone,
		PassengerDtoFirstNameColumnName: d.FirstName,
		PassengerDtoLastNameColumnName:  d.LastName,
	}
}
