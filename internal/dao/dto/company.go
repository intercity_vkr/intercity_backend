package dto

import (
	"github.com/google/uuid"
	"time"
)

const (
	CompanyTableName = "company"
)

const (
	CompanyDtoIdColumnName         = "id"
	CompanyDtoExternalIdColumnName = "external_id"
	CompanyDtoNameColumnName       = "name"
	CompanyDtoAddressColumnName    = "address"
	CompanyDtoCreatedAtColumnName  = "created_at"
	CompanyDtoUpdatedAtColumnName  = "updated_at"
	CompanyDtoDeletedAtColumnName  = "deleted_at"
)

type CompanyDto struct {
	Id         int        `db:"id"`
	ExternalId uuid.UUID  `db:"external_id"`
	Name       string     `db:"name"`
	Address    string     `db:"address"`
	CreatedAt  time.Time  `db:"created_at"`
	UpdatedAt  time.Time  `db:"updated_at"`
	DeletedAt  *time.Time `db:"deleted_at"`
}

func (d CompanyDto) ToSetMap() map[string]any {
	return map[string]any{
		CompanyDtoNameColumnName:    d.Name,
		CompanyDtoAddressColumnName: d.Address,
	}
}
