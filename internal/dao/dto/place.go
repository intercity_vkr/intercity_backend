package dto

import (
	"github.com/google/uuid"
	"time"
)

const (
	PlaceTableName = "place"
)

const (
	PlaceDtoIdColumnName         = "id"
	PlaceDtoExternalIdColumnName = "external_id"
	PlaceDtoNameColumnName       = "name"
	PlaceDtoLatitudeColumnName   = "latitude"
	PlaceDtoLongitudeColumnName  = "longitude"
	PlaceDtoCreatedAtColumnName  = "created_at"
	PlaceDtoUpdatedAtColumnName  = "updated_at"
	PlaceDtoDeletedAtColumnName  = "deleted_at"
)

type PlaceDto struct {
	Id         int        `db:"id"`
	ExternalId uuid.UUID  `db:"external_id"`
	Name       string     `db:"name"`
	Latitude   float64    `db:"latitude"`
	Longitude  float64    `db:"longitude"`
	CreatedAt  time.Time  `db:"created_at"`
	UpdatedAt  time.Time  `db:"updated_at"`
	DeletedAt  *time.Time `db:"deleted_at"`
}

func (d PlaceDto) InsertMap() map[string]any {
	return map[string]any{
		PlaceDtoNameColumnName:      d.Name,
		PlaceDtoLatitudeColumnName:  d.Latitude,
		PlaceDtoLongitudeColumnName: d.Longitude,
	}
}
