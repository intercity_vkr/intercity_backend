package dto

import (
	"github.com/google/uuid"
	"time"
)

const CarTableName = "car"

const (
	CarDtoIdColumnName           = "id"
	CarDtoExternalIdColumnName   = "external_id"
	CarDtoCompanyIdColumnName    = "company_id"
	CarDtoMarkColumnName         = "mark"
	CarDtoModelColumnName        = "model"
	CarDtoGosnomerColumnName     = "gosnomer"
	CarDtoColorColumnName        = "color"
	CarDtoSeatingChartColumnName = "seating_chart"
	CarDtoCreatedAtColumnName    = "created_at"
	CarDtoUpdatedAtColumnName    = "updated_at"
	CarDtoDeletedAtColumnName    = "deleted_at"
)

type CarDto struct {
	Id           int        `db:"id"`
	ExternalId   uuid.UUID  `db:"external_id"`
	CompanyId    int        `db:"company_id"`
	Mark         string     `db:"mark"`
	Model        string     `db:"model"`
	Gosnomer     string     `db:"gosnomer"`
	Color        string     `db:"color"`
	SeatingChart string     `db:"seating_chart"`
	CreatedAt    time.Time  `db:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at"`
	DeletedAt    *time.Time `db:"deleted_at"`
}

func (d CarDto) InsertMap() map[string]any {
	return map[string]any{
		CarDtoCompanyIdColumnName:    d.CompanyId,
		CarDtoMarkColumnName:         d.Mark,
		CarDtoModelColumnName:        d.Model,
		CarDtoGosnomerColumnName:     d.Gosnomer,
		CarDtoColorColumnName:        d.Color,
		CarDtoSeatingChartColumnName: d.SeatingChart,
	}
}

func (d CarDto) UpdateMap() map[string]any {
	return map[string]any{
		CarDtoMarkColumnName:         d.Mark,
		CarDtoModelColumnName:        d.Model,
		CarDtoGosnomerColumnName:     d.Gosnomer,
		CarDtoColorColumnName:        d.Color,
		CarDtoSeatingChartColumnName: d.SeatingChart,
	}
}
