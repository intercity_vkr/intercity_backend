package dto

import (
	"github.com/google/uuid"
	"time"
)

const (
	RouteTableName = "route"
)

const (
	RouteDtoIdColumnName          = "id"
	RouteDtoExternalIdColumnName  = "external_id"
	RouteDtoNameColumnName        = "name"
	RouteDtoDescriptionColumnName = "description"
	RouteDtoCompanyIdColumnName   = "company_id"
	RouteDtoStartPlaceColumnName  = "start_place"
	RouteDtoEndPlaceColumnName    = "end_place"
	RouteDtoCreatedAtColumnName   = "created_at"
	RouteDtoUpdatedAtColumnName   = "updated_at"
	RouteDtoDeletedAtColumnName   = "deleted_at"
)

type Route struct {
	Id           int        `db:"id"`
	ExternalId   uuid.UUID  `db:"external_id"`
	Name         string     `db:"name"`
	Description  string     `db:"description"`
	CompanyId    int        `db:"company_id"`
	StartPlaceId int        `db:"start_place"`
	EndPlaceId   int        `db:"end_place"`
	CreatedAt    time.Time  `db:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at"`
	DeletedAt    *time.Time `db:"deleted_at"`
}

func (r *Route) InsertMap() map[string]any {
	return map[string]any{
		RouteDtoNameColumnName:        r.Name,
		RouteDtoDescriptionColumnName: r.Description,
		RouteDtoCompanyIdColumnName:   r.CompanyId,
	}
}

const (
	RouteStopsTableName = "route_stops"
)

const (
	RouteStopsDtoIdColumnName        = "id"
	RouteStopsDtoRouteIdColumnName   = "route_id"
	RouteStopsDtoPlaceIdColumnName   = "place_id"
	RouteStopsDtoCreatedAtColumnName = "created_at"
	RouteStopsDtoUpdatedAtColumnName = "updated_at"
	RouteStopsDtoDeletedAtColumnName = "deleted_at"
)

type RouteStops struct {
	Id        int        `db:"id"`
	RouteId   int        `db:"route_id"`
	PlaceId   int        `db:"place_id"`
	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt time.Time  `db:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at"`
}

func (r *RouteStops) InsertMap() map[string]any {
	return map[string]any{
		RouteStopsDtoRouteIdColumnName: r.RouteId,
		RouteStopsDtoPlaceIdColumnName: r.PlaceId,
	}
}
