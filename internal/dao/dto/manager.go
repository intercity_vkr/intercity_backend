package dto

import (
	"github.com/google/uuid"
	"time"
)

const ManagerTableName = "manager"

const (
	ManagerDtoIdColumnName           = "id"
	ManagerDtoExternalIdColumnName   = "external_id"
	ManagerDtoPhoneColumnName        = "phone"
	ManagerDtoFirstNameColumnName    = "first_name"
	ManagerDtoLastNameColumnName     = "last_name"
	ManagerDtoCompanyIdColumnName    = "company_id"
	ManagerDtoVerifiedCodeColumnName = "verified_code"
	ManagerDtoVerifiedAtColumnName   = "verified_at"
	ManagerDtoCreatedAtColumnName    = "created_at"
	ManagerDtoUpdatedAtColumnName    = "updated_at"
	ManagerDtoDeletedAtColumnName    = "deleted_at"
)

type ManagerDto struct {
	Id           int        `db:"id"`
	ExternalId   uuid.UUID  `db:"external_id"`
	Phone        string     `db:"phone"`
	FirstName    string     `db:"first_name"`
	LastName     string     `db:"last_name"`
	CompanyId    int        `db:"company_id"`
	VerifiedCode *string    `db:"verified_code"`
	VerifiedAt   *time.Time `db:"verified_at"`
	CreatedAt    time.Time  `db:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at"`
	DeletedAt    *time.Time `db:"deleted_at"`
}

func (d ManagerDto) InsertMap() map[string]any {
	return map[string]any{
		ManagerDtoPhoneColumnName:     d.Phone,
		ManagerDtoFirstNameColumnName: d.FirstName,
		ManagerDtoLastNameColumnName:  d.LastName,
		ManagerDtoCompanyIdColumnName: d.CompanyId,
	}
}

func (d ManagerDto) UpdateMap() map[string]any {
	return map[string]any{
		ManagerDtoPhoneColumnName:        d.Phone,
		ManagerDtoFirstNameColumnName:    d.FirstName,
		ManagerDtoLastNameColumnName:     d.LastName,
		ManagerDtoVerifiedCodeColumnName: d.VerifiedCode,
	}
}
