package dao

import (
	"context"
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	crf "intercity_backend/gen/ent/car"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/mappers"
)

type Car struct {
	db *ent.CarClient
}

func NewCarDao(db *ent.CarClient) *Car {
	return &Car{db: db}
}

func (d *Car) CreateCar(ctx context.Context, car *entities.CarEntity) (*entities.CarEntity, error) {
	carDto, err := d.db.Create().
		SetMark(car.Mark).
		SetModel(car.Model).
		SetGosnomer(car.Gosnomer).
		SetColor(car.Color).
		SetSeatingChart(car.SeatingChart).
		SetCompanyID(car.CompanyId).Save(ctx)
	if err != nil {
		return nil, err
	}
	c, err := d.db.Query().WithCompany().Where(crf.ID(carDto.ID)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.CarDtoToEntity(c), nil
}

func (d *Car) GetByFilter(ctx context.Context, filter *filters.CarFilter) ([]*entities.CarEntity, error) {
	carDtos, err := d.db.Query().WithCompany().Where(filter.GetPredicates()...).All(ctx)
	if err != nil {
		return nil, err
	}
	cars := make([]*entities.CarEntity, 0, len(carDtos))
	for _, carDto := range carDtos {
		cars = append(cars, mappers.CarDtoToEntity(carDto))
	}
	return cars, nil
}

func (d *Car) Update(ctx context.Context, car *entities.CarEntity) error {
	_, err := d.db.UpdateOneID(car.Id).
		SetMark(car.Mark).
		SetModel(car.Model).
		SetGosnomer(car.Gosnomer).
		SetColor(car.Color).
		SetSeatingChart(car.SeatingChart).
		Save(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (d *Car) Delete(ctx context.Context, extId uuid.UUID) error {
	_, err := d.db.Delete().Where(crf.ExternalID(extId)).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}
