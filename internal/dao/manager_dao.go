package dao

import (
	"context"
	"intercity_backend/gen/ent"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/filters"
	"intercity_backend/internal/mappers"
)

type Manager struct {
	db *ent.ManagerClient
}

func NewManagerDao(db *ent.ManagerClient) *Manager {
	return &Manager{db: db}
}

func (d *Manager) CreateManager(ctx context.Context, manager *entities.ManagerEntity) (*entities.ManagerEntity, error) {
	managerDto, err := d.db.Create().SetPhone(manager.Phone).
		SetFirstName(manager.FirstName).
		SetLastName(manager.LastName).
		SetCompanyID(int(manager.CompanyId)).Save(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.ManagerDtoToEntity(managerDto), nil
}

func (d *Manager) GetByFilter(ctx context.Context, filter *filters.ManagerFilter) (*entities.ManagerEntity, error) {
	managerDto, err := d.db.Query().WithCompany().Where(filter.GetPredicates()...).Only(ctx)
	if err != nil {
		return nil, err
	}
	return mappers.ManagerDtoToEntity(managerDto), nil
}

func (d *Manager) Update(ctx context.Context, entity *entities.ManagerEntity) error {
	query := d.db.UpdateOneID(int(entity.Id)).
		SetFirstName(entity.FirstName).
		SetLastName(entity.LastName)
	if entity.VerifiedCode == nil {
		query = query.ClearVerifiedCode()
	} else {
		query = query.SetVerifiedCode(*entity.VerifiedCode)
	}

	_, err := query.Save(ctx)

	if err != nil {
		return err
	}
	return nil
}
