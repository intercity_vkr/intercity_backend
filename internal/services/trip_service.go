package services

import (
	"context"
	"intercity_backend/internal/dao"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/utils"
)

type Trip struct {
	tripDao *dao.Trip
}

func NewTripService(tripDao *dao.Trip) *Trip {
	return &Trip{tripDao: tripDao}
}

func (t *Trip) BookSeatInTrip(ctx context.Context, tripId int, passengerId int, seatNumber int) (*entities.Trip, error) {
	trip, err := t.tripDao.GetById(ctx, tripId)
	if err != nil {
		return nil, err
	}
	if !trip.OccupySeat(seatNumber) {
		return trip, utils.WrapError(internal_errors.ErrInvalidData, "this seat is already occupied")
	}
	err = t.tripDao.AddPassenger(ctx, tripId, passengerId, seatNumber, trip.FullPrice, trip.Route.EndPlace.Id)
	if err != nil {
		return trip, err
	}
	trip, err = t.tripDao.Update(ctx, trip)
	if err != nil {
		return trip, err
	}
	return trip, nil
}
