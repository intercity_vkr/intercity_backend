package services

import (
	"fmt"
	"github.com/google/uuid"
	"intercity_backend/internal/app/config"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/entities/ids"
	"intercity_backend/internal/internal_errors"
	"intercity_backend/pkg/jwt"
	"time"
)

type Jwt struct {
	config *config.Config
}

func NewJwtService(config *config.Config) *Jwt {
	return &Jwt{config: config}
}

func (j *Jwt) NewJwtTokenPairs(userType entities.UserType, externalId uuid.UUID) (*entities.TokenPair, error) {
	accessTokenTl, err := time.ParseDuration(j.config.HttpAuthorization.AccessTokenTL)
	if err != nil {
		panic(err)
	}
	refreshTokenTl, err := time.ParseDuration(j.config.HttpAuthorization.RefreshTokenTL)
	if err != nil {
		panic(err)
	}
	switch userType {
	case entities.PassengerUserType:
		accessToken, err := jwt.NewJwtWithClaims(j.config.HttpAuthorization.SecretKey,
			entities.NewPassengerClaims(ids.PassengerExternalId(externalId), accessTokenTl, entities.AccessTokenType))
		if err != nil {
			return nil, err
		}
		refreshToken, err := jwt.NewJwtWithClaims(j.config.HttpAuthorization.SecretKey,
			entities.NewPassengerClaims(ids.PassengerExternalId(externalId), refreshTokenTl, entities.RefreshTokenType))
		if err != nil {
			return nil, err
		}
		return &entities.TokenPair{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		}, nil
	case entities.ManagerUserType:
		accessToken, err := jwt.NewJwtWithClaims(j.config.HttpAuthorization.SecretKey,
			entities.NewManagerClaims(ids.ManagerExternalId(externalId), accessTokenTl, entities.AccessTokenType))
		if err != nil {
			return nil, err
		}
		refreshToken, err := jwt.NewJwtWithClaims(j.config.HttpAuthorization.SecretKey,
			entities.NewManagerClaims(ids.ManagerExternalId(externalId), refreshTokenTl, entities.RefreshTokenType))
		if err != nil {
			return nil, err
		}
		return &entities.TokenPair{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		}, nil
	default:
		panic(fmt.Sprintf("unknown user type %s", userType))
	}
}

func (j *Jwt) ParseToken(rawToken string) (*entities.UserClaims, error) {
	claims, err := jwt.ParseJwt(rawToken, j.config.HttpAuthorization.SecretKey, &entities.UserClaims{})
	if err != nil {
		return nil, internal_errors.ErrUnauthorized
	}
	userClaims, ok := claims.(*entities.UserClaims)
	if !ok {
		return nil, internal_errors.ErrUnauthorized
	}
	return userClaims, nil
}
