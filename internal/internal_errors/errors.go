package internal_errors

import "errors"

// 404

var ErrNotFound = errors.New("not found")

// 401

var ErrUnauthorized = errors.New("unauthorized")

// 400

var ErrInvalidData = errors.New("invalid data")

var ErrInvalidBody = errors.New("invalid body")
var ErrAlreadyExist = errors.New("already exist")
