package internal_errors

import (
	"context"
	"errors"
	"intercity_backend/pkg/core/httpserver"
)

func InternalToHttpError() httpserver.ErrorMapperFunc {
	return func(ctx context.Context, err error) httpserver.HttpError {
		switch {
		case errors.Is(err, ErrNotFound):
			return httpserver.NewErrorNotFound(err.Error())
		case errors.Is(err, ErrInvalidBody),
			errors.Is(err, ErrAlreadyExist),
			errors.Is(err, ErrInvalidData):
			return httpserver.NewErrorBadRequest(err.Error())
		default:
			return httpserver.NewErrorInternalError(err.Error())
		}
	}
}
