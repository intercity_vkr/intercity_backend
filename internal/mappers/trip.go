package mappers

import (
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/entities"
)

func TripDtoToEntity(dto *ent.Trip) *entities.Trip {
	return &entities.Trip{
		Id:           dto.ID,
		CompanyId:    dto.CompanyID,
		RouteId:      dto.RouteID,
		Reversed:     dto.Reversed,
		CarId:        dto.CarID,
		StartTime:    dto.StartTime,
		EndTime:      dto.EndTime,
		FullPrice:    dto.FullPrice,
		SeatingChart: dto.SeatingChart,
		ExternalId:   dto.ExternalID,
		Company:      *CompanyDtoToEntity(dto.Edges.Company),
		Route:        *RouteDtoToEntity(dto.Edges.Route),
		Car:          *CarDtoToEntity(dto.Edges.Car),
		Passengers: func() []entities.TripPassenger {
			var passengers []entities.TripPassenger
			for _, p := range dto.Edges.TripPassengers {
				passengers = append(passengers, *TripPassengerDtoToEntity(p))
			}
			return passengers
		}(),
	}
}

func TripPassengerDtoToEntity(dto *ent.TripPassenger) *entities.TripPassenger {
	return &entities.TripPassenger{
		Id:         dto.ID,
		TripId:     dto.TripID,
		SeatNumber: dto.SeatNumber,
		Approved:   dto.Approved,
		Passenger:  *PassengerDtoToEntity(dto.Edges.Passenger),
		EndPlace:   *PlaceDtoToEntity(dto.Edges.EndPlace),
	}
}

func TripPassengerEntityToHttp(entity *entities.TripPassenger) *openapi.TripPassenger {
	return &openapi.TripPassenger{
		Approved:   entity.Approved,
		EndPlace:   *PlaceEntityToHttp(&entity.EndPlace),
		Passenger:  *PassengerEntityToHttp(&entity.Passenger),
		Price:      entity.Price,
		SeatNumber: entity.SeatNumber,
	}
}

func TripPassengerEntitiesToHttp(v []entities.TripPassenger) []openapi.TripPassenger {
	resp := make([]openapi.TripPassenger, 0, len(v))
	for _, e := range v {
		resp = append(resp, *TripPassengerEntityToHttp(&e))
	}
	return resp
}

func TripEntityToHttp(entity *entities.Trip) *openapi.Trip {
	return &openapi.Trip{
		TripId:        entity.ExternalId.String(),
		Reversed:      entity.Reversed,
		StartTime:     entity.StartTime,
		EndTime:       entity.EndTime,
		Company:       *CompanyEntityToHttp(&entity.Company),
		Route:         *RouteEntityToHttp(&entity.Route),
		Car:           *CarEntityToHttp(&entity.Car),
		TripPassenger: TripPassengerEntitiesToHttp(entity.Passengers),
		FullPrice:     entity.FullPrice,
		SeatingChart:  entity.SeatingChart,
	}
}
