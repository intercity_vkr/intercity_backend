package mappers

import (
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/entities"
)

func RouteDtoToEntity(route *ent.Route) *entities.Route {
	return &entities.Route{
		Id:          route.ID,
		ExternalId:  route.ExternalID,
		Name:        route.Name,
		Description: route.Description,
		CompanyId:   route.CompanyID,
		StartPlace:  *PlaceDtoToEntity(route.Edges.StartPlace),
		EndPlace:    *PlaceDtoToEntity(route.Edges.EndPlace),
		Stops: func() []entities.Place {
			stops := make([]entities.Place, 0, len(route.Edges.Stops))
			for _, stop := range route.Edges.Stops {
				stops = append(stops, *PlaceDtoToEntity(stop))
			}
			return stops
		}(),
		CreatedAt: route.CreatedAt,
		UpdatedAt: route.UpdatedAt,
		DeletedAt: route.DeletedAt,
	}
}

func RouteEntityToHttp(route *entities.Route) *openapi.Route {
	return &openapi.Route{
		RouteId:     route.ExternalId.String(),
		Name:        route.Name,
		Description: route.Description,
		StartPlace:  *PlaceEntityToHttp(&route.StartPlace),
		EndPlace:    *PlaceEntityToHttp(&route.EndPlace),
		Stops: func() []openapi.Place {
			stops := make([]openapi.Place, 0, len(route.Stops))
			for _, stop := range route.Stops {
				stops = append(stops, *PlaceEntityToHttp(&stop))
			}
			return stops
		}(),
	}
}
