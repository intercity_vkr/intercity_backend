package mappers

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/entities/ids"
)

func CompanyEntityToDto(entity *entities.CompanyEntity) *dto.CompanyDto {
	return &dto.CompanyDto{
		Id:         int(entity.Id),
		ExternalId: uuid.UUID(entity.ExternalId),
		Name:       entity.Name,
		Address:    entity.Address,
		CreatedAt:  entity.CreatedAt,
		UpdatedAt:  entity.UpdatedAt,
		DeletedAt:  entity.DeletedAt,
	}
}

func CompanyDtoToEntity(dto *ent.Company) *entities.CompanyEntity {
	return &entities.CompanyEntity{
		Id:         ids.Company(dto.ID),
		ExternalId: ids.CompanyExternalId(uuid.UUID(dto.ExternalID)),
		Name:       dto.Name,
		Address:    dto.Address,
		CreatedAt:  dto.CreatedAt,
		UpdatedAt:  dto.UpdatedAt,
		DeletedAt:  dto.DeletedAt,
	}
}

func CompanyEntityToHttp(company *entities.CompanyEntity) *openapi.Company {
	if company == nil {
		return nil
	}
	return &openapi.Company{
		Address:   company.Address,
		CompanyId: uuid.UUID(company.ExternalId).String(),
		Name:      company.Name,
	}
}
