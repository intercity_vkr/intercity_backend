package mappers

import (
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/entities"
)

func TokenPairEntityToHttp(pair entities.TokenPair) *openapi.TokenPair {
	return &openapi.TokenPair{
		AccessToken:  pair.AccessToken,
		RefreshToken: pair.RefreshToken,
	}
}
