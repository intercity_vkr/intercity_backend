package mappers

import (
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/entities/ids"
)

func PassengerEntityToDto(entity *entities.PassengerEntity) *dto.PassengerDto {
	return &dto.PassengerDto{
		Phone:     entity.Phone,
		FirstName: entity.FirstName,
		LastName:  entity.LastName,
	}
}

func PassengerDtoToEntity(dto *ent.Passenger) *entities.PassengerEntity {
	return &entities.PassengerEntity{
		Id:           ids.Passenger(dto.ID),
		Phone:        dto.Phone,
		FirstName:    dto.FirstName,
		LastName:     dto.LastName,
		ExternalId:   dto.ExternalID,
		Ban:          dto.Ban,
		VerifiedCode: dto.VerifiedCode,
		VerifiedAt:   dto.VerifiedAt,
		CreatedAt:    dto.CreatedAt,
		UpdatedAt:    dto.UpdatedAt,
		DeletedAt:    dto.DeletedAt,
	}
}

func PassengerIdToHttp(entity *entities.PassengerEntity) *openapi.PassengerAuthInitResponse {
	return &openapi.PassengerAuthInitResponse{PassengerId: entity.ExternalId.String()}
}

func PassengerSelfToHttp(entity *entities.PassengerEntity) *openapi.PassengerSelfResponse {
	return &openapi.PassengerSelfResponse{
		PassengerId: entity.ExternalId.String(),
		FirstName:   entity.FirstName,
		LastName:    entity.LastName,
		Phone:       entity.Phone,
	}
}

func PassengerEntityToHttp(entity *entities.PassengerEntity) *openapi.Passenger {
	return &openapi.Passenger{
		FirstName:   entity.FirstName,
		LastName:    entity.LastName,
		PassengerId: entity.ExternalId.String(),
		Phone:       entity.Phone,
	}
}
