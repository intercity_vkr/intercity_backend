package mappers

import (
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities"
)

func PlaceEntityToDto(place *entities.Place) *dto.PlaceDto {
	return &dto.PlaceDto{
		Id:         place.Id,
		ExternalId: place.ExternalId,
		Name:       place.Name,
		Latitude:   place.Latitude,
		Longitude:  place.Longitude,
		CreatedAt:  place.CreatedAt,
		UpdatedAt:  place.UpdatedAt,
		DeletedAt:  place.DeletedAt,
	}
}

func PlaceDtoToEntity(placeDto *ent.Place) *entities.Place {
	return &entities.Place{
		Id:         placeDto.ID,
		ExternalId: placeDto.ExternalID,
		Name:       placeDto.Name,
		Latitude:   placeDto.Latitude,
		Longitude:  placeDto.Longitude,
		CreatedAt:  placeDto.CreatedAt,
		UpdatedAt:  placeDto.UpdatedAt,
		DeletedAt:  placeDto.DeletedAt,
	}
}

func PlaceEntityToHttp(place *entities.Place) *openapi.Place {
	return &openapi.Place{
		PlaceId: place.ExternalId.String(),
		Name:    place.Name,
		Point: openapi.Point{
			Latitude:  float32(place.Latitude),
			Longitude: float32(place.Longitude),
		},
	}
}
