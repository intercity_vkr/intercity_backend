package mappers

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities"
	"intercity_backend/internal/entities/ids"
)

func ManagerEntityToDto(entity *entities.ManagerEntity) *dto.ManagerDto {
	return &dto.ManagerDto{
		Id:           int(entity.Id),
		ExternalId:   uuid.UUID(entity.ExternalId),
		Phone:        entity.Phone,
		FirstName:    entity.FirstName,
		LastName:     entity.LastName,
		CompanyId:    int(entity.CompanyId),
		VerifiedCode: entity.VerifiedCode,
		VerifiedAt:   entity.VerifiedAt,
		CreatedAt:    entity.CreatedAt,
		UpdatedAt:    entity.UpdatedAt,
		DeletedAt:    entity.DeletedAt,
	}
}

func ManagerDtoToEntity(dto *ent.Manager) *entities.ManagerEntity {
	return &entities.ManagerEntity{
		Id:           ids.Manager(dto.ID),
		ExternalId:   ids.ManagerExternalId(dto.ExternalID),
		Phone:        dto.Phone,
		FirstName:    dto.FirstName,
		LastName:     dto.LastName,
		CompanyId:    ids.Company(dto.CompanyID),
		VerifiedCode: dto.VerifiedCode,
		VerifiedAt:   dto.VerifiedAt,
		CreatedAt:    dto.CreatedAt,
		UpdatedAt:    dto.UpdatedAt,
		DeletedAt:    dto.DeletedAt,
		Company:      *CompanyDtoToEntity(dto.Edges.Company),
	}
}

func ManagerEntityToHttp(manager *entities.ManagerEntity, company *entities.CompanyEntity) *openapi.Manager {
	return &openapi.Manager{
		Company:   CompanyEntityToHttp(company),
		FirstName: manager.FirstName,
		LastName:  manager.LastName,
		ManagerId: uuid.UUID(manager.ExternalId).String(),
		Phone:     manager.Phone,
	}
}
