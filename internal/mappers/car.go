package mappers

import (
	"intercity_backend/gen/ent"
	"intercity_backend/gen/openapi"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities"
)

func CarEntityToDto(car *entities.CarEntity) *dto.CarDto {
	return &dto.CarDto{
		Id:           car.Id,
		ExternalId:   car.ExternalId,
		CompanyId:    car.CompanyId,
		Mark:         car.Mark,
		Model:        car.Model,
		Gosnomer:     car.Gosnomer,
		Color:        car.Color,
		SeatingChart: car.SeatingChart,
		CreatedAt:    car.CreatedAt,
		UpdatedAt:    car.UpdatedAt,
		DeletedAt:    car.DeletedAt,
	}
}

func CarDtoToEntity(car *ent.Car) *entities.CarEntity {
	return &entities.CarEntity{
		Id:           car.ID,
		ExternalId:   car.ExternalID,
		Mark:         car.Mark,
		Model:        car.Model,
		Gosnomer:     car.Gosnomer,
		Color:        car.Color,
		SeatingChart: car.SeatingChart,
		CreatedAt:    car.CreatedAt,
		UpdatedAt:    car.UpdatedAt,
		DeletedAt:    car.DeletedAt,
		Company: func() entities.CompanyEntity {
			c := CompanyDtoToEntity(car.Edges.Company)
			if c == nil {
				return entities.CompanyEntity{}
			}
			return *c
		}(),
	}
}

func CarEntityToHttp(car *entities.CarEntity) *openapi.Car {
	return &openapi.Car{
		CarId:        car.ExternalId.String(),
		Mark:         car.Mark,
		Model:        car.Model,
		Gosnomer:     car.Gosnomer,
		Color:        car.Color,
		SeatingChart: car.SeatingChart,
		Company:      *CompanyEntityToHttp(&car.Company),
	}
}
