package filters

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent/manager"
	"intercity_backend/gen/ent/predicate"
)

type ManagerFilter struct {
	Ids         []int
	ExternalIds []uuid.UUID
	Phone       *string
	FirstName   *string
	LastName    *string
	CompanyIds  []int
}

func (f *ManagerFilter) GetPredicates() []predicate.Manager {
	var predicates []predicate.Manager
	if len(f.Ids) > 0 {
		predicates = append(predicates, manager.IDIn(f.Ids...))
	}
	if len(f.ExternalIds) > 0 {
		predicates = append(predicates, manager.ExternalIDIn(f.ExternalIds...))
	}
	if f.Phone != nil {
		predicates = append(predicates, manager.Phone(*f.Phone))
	}
	if f.FirstName != nil {
		predicates = append(predicates, manager.FirstName(*f.FirstName))
	}
	if f.LastName != nil {
		predicates = append(predicates, manager.LastName(*f.LastName))
	}
	if len(f.CompanyIds) > 0 {
		predicates = append(predicates, manager.CompanyIDIn(f.CompanyIds...))
	}
	return predicates
}
