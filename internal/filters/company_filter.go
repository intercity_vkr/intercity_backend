package filters

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent/company"
	"intercity_backend/gen/ent/predicate"
)

type CompanyFilter struct {
	Ids         []int
	ExternalIds []uuid.UUID
	Name        *string
}

func (f *CompanyFilter) GetPredicates() []predicate.Company {
	var predicates []predicate.Company
	if len(f.Ids) > 0 {
		predicates = append(predicates, company.IDIn(f.Ids...))
	}
	if len(f.ExternalIds) > 0 {
		predicates = append(predicates, company.ExternalIDIn(f.ExternalIds...))
	}
	if f.Name != nil {
		predicates = append(predicates, company.Name(*f.Name))
	}
	return predicates
}
