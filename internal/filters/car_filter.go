package filters

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent/car"
	"intercity_backend/gen/ent/predicate"
)

type CarFilter struct {
	Ids          []int
	ExternalIds  []uuid.UUID
	CompaniesIds []int
	Mark         *string
	Model        *string
	Gosnomer     *string
	Color        *string
	SeatingChart *string
}

func (f *CarFilter) GetPredicates() []predicate.Car {
	var predicates []predicate.Car
	if len(f.Ids) > 0 {
		predicates = append(predicates, car.IDIn(f.Ids...))
	}
	if len(f.ExternalIds) > 0 {
		predicates = append(predicates, car.ExternalIDIn(f.ExternalIds...))
	}
	if len(f.CompaniesIds) > 0 {
		predicates = append(predicates, car.CompanyIDIn(f.CompaniesIds...))
	}
	if f.Mark != nil {
		predicates = append(predicates, car.Mark(*f.Mark))
	}
	if f.Model != nil {
		predicates = append(predicates, car.Model(*f.Model))
	}
	if f.Gosnomer != nil {
		predicates = append(predicates, car.Gosnomer(*f.Gosnomer))
	}
	if f.Color != nil {
		predicates = append(predicates, car.Color(*f.Color))
	}
	if f.SeatingChart != nil {
		predicates = append(predicates, car.SeatingChart(*f.SeatingChart))
	}
	return predicates
}
