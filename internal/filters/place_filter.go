package filters

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent/place"
	"intercity_backend/gen/ent/predicate"
)

type PlaceFilter struct {
	Ids         []int
	ExternalIds []uuid.UUID
	Name        *string
}

func (f *PlaceFilter) GetPredicates() []predicate.Place {
	var predicates []predicate.Place
	if len(f.Ids) > 0 {
		predicates = append(predicates, place.IDIn(f.Ids...))
	}
	if len(f.ExternalIds) > 0 {
		predicates = append(predicates, place.ExternalIDIn(f.ExternalIds...))
	}
	if f.Name != nil {
		predicates = append(predicates, place.Name(*f.Name))
	}
	return predicates
}
