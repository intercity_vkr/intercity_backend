package filters

import (
	"github.com/Masterminds/squirrel"
	"intercity_backend/internal/dao/dto"
	"intercity_backend/internal/entities/ids"
)

type PassengerFilter struct {
	Ids         ids.Passengers
	Phones      []string
	FirstName   *string
	LastName    *string
	ExternalIds ids.PassengerExternalIds
	Ban         *bool
}

func (f *PassengerFilter) Sql(q squirrel.SelectBuilder) squirrel.SelectBuilder {
	if len(f.Ids) > 0 {
		q = q.Where(squirrel.Eq{dto.PassengerDtoIdColumnName: f.Ids})
	}
	if len(f.Phones) > 0 {
		q = q.Where(squirrel.Eq{dto.PassengerDtoPhoneColumnName: f.Phones})
	}
	if f.FirstName != nil {
		q = q.Where(squirrel.Eq{dto.PassengerDtoFirstNameColumnName: f.FirstName})
	}
	if f.LastName != nil {
		q = q.Where(squirrel.Eq{dto.PassengerDtoLastNameColumnName: f.LastName})
	}
	if len(f.ExternalIds) > 0 {
		q = q.Where(squirrel.Eq{dto.PassengerDtoExternalIdColumnName: f.ExternalIds})
	}
	if f.Ban != nil {
		q = q.Where(squirrel.Eq{dto.PassengerDtoBanColumnName: f.Ban})
	}
	return q
}
