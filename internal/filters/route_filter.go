package filters

import (
	"github.com/google/uuid"
	"intercity_backend/gen/ent/place"
	"intercity_backend/gen/ent/predicate"
	"intercity_backend/gen/ent/route"
)

type RouteFilter struct {
	Ids          []int
	ExternalIds  []uuid.UUID
	CompanyIds   []int
	Name         *string
	StartPlaceId *int
	EndPlaceId   *int
	StopsId      []int
}

func (f *RouteFilter) GetPredicates() []predicate.Route {
	var predicates []predicate.Route
	if len(f.Ids) > 0 {
		predicates = append(predicates, route.IDIn(f.Ids...))
	}
	if len(f.ExternalIds) > 0 {
		predicates = append(predicates, route.ExternalIDIn(f.ExternalIds...))
	}
	if len(f.CompanyIds) > 0 {
		predicates = append(predicates, route.CompanyIDIn(f.CompanyIds...))
	}
	if f.Name != nil {
		predicates = append(predicates, route.Name(*f.Name))
	}
	if f.StartPlaceId != nil {
		predicates = append(predicates, route.StartPlaceID(*f.StartPlaceId))
	}
	if f.EndPlaceId != nil {
		predicates = append(predicates, route.EndPlaceID(*f.EndPlaceId))
	}
	if len(f.StopsId) > 0 {
		predicates = append(predicates, route.HasStopsWith(place.IDIn(f.StopsId...)))
	}
	return predicates
}
