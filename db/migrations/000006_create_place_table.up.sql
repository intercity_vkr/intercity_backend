CREATE TABLE places
(
    id          serial PRIMARY KEY,
    external_id uuid UNIQUE NOT NULL DEFAULT gen_random_uuid(),
    name        text        NOT NULL,
    longitude   numeric     NOT NULL,
    latitude    numeric     NOT NULL,
    created_at  timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at  timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at  timestamptz          DEFAULT NULL
)