CREATE TABLE cars
(
    id            serial PRIMARY KEY,
    external_id   uuid UNIQUE NOT NULL DEFAULT gen_random_uuid(),
    company_id    integer REFERENCES companies,
    mark          text        not null,
    model         text        not null,
    gosnomer      text        not null,
    color         text        not null,
    seating_chart text        not null,
    created_at    timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at    timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at    timestamptz          DEFAULT NULL
);