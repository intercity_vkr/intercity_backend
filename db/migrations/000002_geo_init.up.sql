-- CREATE TABLE countries
-- (
--     id          SERIAL PRIMARY KEY,
--     external_id uuid UNIQUE  NOT NULL DEFAULT gen_random_uuid(),
--     name        varchar(255) NOT NULL UNIQUE,
--     code        varchar(2)   NOT NULL UNIQUE,
--     created_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     updated_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     deleted_at  timestamptz           DEFAULT NULL
-- );
--
-- CREATE TABLE regions
-- (
--     id          SERIAL PRIMARY KEY,
--     external_id uuid UNIQUE  NOT NULL DEFAULT gen_random_uuid(),
--     name        varchar(255) NOT NULL UNIQUE,
--     country_id  integer      NOT NULL REFERENCES countries,
--     created_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     updated_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     deleted_at  timestamptz           DEFAULT NULL
-- );
--
-- CREATE TABLE sub_regions
-- (
--     id          SERIAL PRIMARY KEY,
--     external_id uuid UNIQUE  NOT NULL DEFAULT gen_random_uuid(),
--     name        varchar(255) NOT NULL UNIQUE,
--     region_id   integer      NOT NULL REFERENCES regions,
--     created_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     updated_at  timestamptz  NOT NULL DEFAULT current_timestamp,
--     deleted_at  timestamptz           DEFAULT NULL
-- );
--
-- CREATE TABLE places
-- (
--     id            SERIAL PRIMARY KEY,
--     external_id   uuid UNIQUE         NOT NULL DEFAULT gen_random_uuid(),
--     name          varchar(255) UNIQUE NOT NULL,
--     sub_region_id integer REFERENCES sub_regions,
--     region_id     integer             NOT NULL REFERENCES regions,
--     point         geometry            NOT NULL,
--     created_at    timestamptz         NOT NULL DEFAULT current_timestamp,
--     updated_at    timestamptz         NOT NULL DEFAULT current_timestamp,
--     deleted_at    timestamptz                  DEFAULT NULL
-- )
