CREATE TABLE managers
(
    id            serial PRIMARY KEY,
    external_id   uuid UNIQUE        NOT NULL DEFAULT gen_random_uuid(),
    phone         varchar(11) UNIQUE NOT NULL,
    first_name    varchar(255)       NOT NULL,
    last_name     varchar(255)       NOT NULL,
    company_id    integer            NOT NULL REFERENCES companies,
    verified_code varchar(4)                  DEFAULT NULL,
    verified_at   timestamptz                 DEFAULT NULL,
    created_at    timestamptz        NOT NULL DEFAULT current_timestamp,
    updated_at    timestamptz        NOT NULL DEFAULT current_timestamp,
    deleted_at    timestamptz                 DEFAULT NULL
);