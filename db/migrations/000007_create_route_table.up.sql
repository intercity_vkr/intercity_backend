CREATE TABLE routes
(
    id             serial PRIMARY KEY,
    external_id    uuid UNIQUE NOT NULL DEFAULT gen_random_uuid(),
    name           text        NOT NULL,
    description    text,
    company_id     integer     NOT NULL REFERENCES companies,
    start_place_id integer     NOT NULL REFERENCES places,
    end_place_id   integer     NOT NULL REFERENCES places,
    created_at     timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at     timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at     timestamptz          DEFAULT NULL
);

CREATE TABLE route_stops
(
    id         serial PRIMARY KEY,
    route_id   integer     NOT NULL REFERENCES routes,
    place_id   integer     NOT NULL REFERENCES places,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at timestamptz          DEFAULT NULL
);