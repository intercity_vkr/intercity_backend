CREATE TABLE trips
(
    id            serial PRIMARY KEY,
    external_id   uuid UNIQUE NOT NULL DEFAULT gen_random_uuid(),
    company_id    integer     NOT NULL REFERENCES companies,
    route_id      integer     NOT NULL REFERENCES routes,
    reversed      boolean     NOT NULL DEFAULT false,
    car_id        integer     NOT NULL REFERENCES cars,
    start_time    timestamptz NOT NULL,
    end_time      timestamptz NOT NULL,
    full_price    integer     NOT NULL,
    seating_chart text        NOT NULL,
    created_at    timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at    timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at    timestamptz          DEFAULT NULL
);

CREATE TABLE trip_passengers
(
    id           serial PRIMARY KEY,
    trip_id      integer     NOT NULL REFERENCES trips,
    passenger_id integer     NOT NULL REFERENCES passengers,
    seat_number  integer,
    price        integer,
    end_place_id integer     NOT NULL REFERENCES places,
    approved     boolean     NOT NULL DEFAULT false,
    created_at   timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at   timestamptz NOT NULL DEFAULT current_timestamp,
    deleted_at   timestamptz          DEFAULT NULL
);